<?php


Route::get('/', function () {
    return view('welcome');
})->name('regresar');

Route::get('plantillaCE',function(){
    return view('plantilla-Control-Esc');
});

Route::get('/Creditos-Equipo-Desarrollo',function(){
    return view('layouts/Creditos');
})->name('creditos');




/*Rutas de Inicio de sesión de los usuarios del sistema*/

Route::get('/indexControlEsc', function(){
    return view('controlEscolar/inicioControlEsc');
})->name('inicioCE');

Route::get('/indexCoordinador', function(){
    return view('coordinador/inicioCoordinadores');
})->name('inicioC');

Route::get('/indexProfesor', function(){
    return view('profesor/inicioProfesores');
})->name('inicioP');

Route::get('indexAlumno', function(){
    return view('alumno/inicioAlumnos');
})->name('inicioA');

Route::get('profesores/calificacion', function(){
    return view('profesor/calificacionesProfesores');
})->name('calificacion');

Route::get('profesores/horario', function(){
    return view('profesor/HorarioProfesores');
})->name('HO');

Route::get('profesores/adeudos', function(){
    return view('profesor/adeudosProfesores');
})->name('ADE');

Route::get('profesores/plan', function(){
    return view('profesor/planProfesores');
})->name('Plan');

Route::get('profesores/kardex', function(){
    return view('profesor/kardexProfesores');
})->name('KADX');



/***************************/

/*Rutas de Login de cada usuario*/
Route::get('/principal/login-students', function(){
    return view('login/login-user-student');
})->name('login-s');

Route::get('/principal/login-coordinator', function(){
    return view('login/login-user-coor');
})->name('login-coor');

Route::get('/principal/login-control', function(){
    return view('login/login-user-controlesc');
})->name('login-coe');

Route::get('/principal/login-control-escolar', function(){
    return view('login/login-user-teacher');
})->name('login-t');

/**********/
/**Ruta de Acceso Rapido unico Control Escolar**/
Route::get('/controlEsc/AccesFast', function(){
    return view('controlEscolar/accesoRapido');
})->name('acceso');

/***********/

/**Rutas de Registros de usuarios dentro del sistema**/


Route::get('/controlEsc/formC', function(){
    return view('controlEscolar/formularioCoordinadores');
})->name('FC');


Route::get('/controlEsc', function(){
    return view('controlEscolar/calificacionesA');
})->name('PA');

/*Edicion en tabla a usuarios*/

/*Alumnos*/
Route::get('/controlEsc/Alumnos', 'AlumnosController@create')->name('FA');
Route::post('/controlEsc/Alumnos', 'AlumnosController@store')->name('FA');
Route::get('/controlEsc/edicionAlumnos', 'AlumnosController@index')->name('EAT');
Route::get('/controlEsc/editarAlumnos{Matricula}', 'AlumnosController@edit')->name('EEAT');
Route::post('/controlEsc/editarAlumnos{Matricula}', 'AlumnosController@update')->name('EFA');
Route::get('/controlEsc/eliminarAlumnos{Matricula}', 'AlumnosController@destroy')->name('EA');

Route::get('/alumno/Perfil', function(){
    return view('alumno/perfilAlumno');
})->name('perfilAlumno');

Route::get('/alumno/Contacto', function(){
    return view('alumno/contactoAlumno');
})->name('contactoAlumno');

Route::get('/alumno/Calificacion', function(){
    return view('alumno/calificacionAlumno');
})->name('calificacionAlumno');

Route::get('/alumno/Adeudos', function(){
    return view('alumno/adeudosAlumno');
})->name('adeudosAlumno');

Route::get('/alumno/Inicio', function(){
    return view('alumno/inicioAlumnos');
})->name('inicioAlumno');

Route::get('/alumno/Horario', function(){
    return view('alumno/horarioAlumno');
})->name('horarioAlumno');

Route::get('/alumno/Plan-Estudios', function(){
    return view('alumno/planAlumno');
})->name('planAlumno');

Route::get('/alumno/Kardex', function(){
    return view('alumno/kardexAlumno');
})->name('kardexAlumno');



/*Fin Alumnos*/

/*Profesores*/
Route::get('/controlEsc/Profesores', 'ProfesorController@create')->name('FP');
Route::post('/controlEsc/Profesores', 'ProfesorController@store')->name('FP');
Route::get('/controlEsc/edicionProfesores', 'ProfesorController@index')->name('EPT');
Route::get('/controlEsc/editarProfesores{Clave_profesor}', 'ProfesorController@edit')->name('EEPT');
Route::post('/controlEsc/editarProfesores{Clave_profesor}', 'ProfesorController@update')->name('EFP');
Route::get('/controlEsc/eliminarProfesores{Clave_profesor}', 'ProfesorController@destroy')->name('EP');
Route::get('/profesor/Perfil', function(){
    return view('profesor/perfilProfesor');
})->name('Perso');

Route::get('/profesor/Califica', function(){
    return view('profesor/captura-califProfesor');
})->name('calificaProfesor');

Route::get('/profesor/Historico', function(){
    return view('profesor/historicoProfesor');
})->name('historicoProfesor');

Route::get('/profesor/Historico-Alumnos', function(){
    return view('profesor/lista-alumnoProfesor');
})->name('listasProfesor');



/*Fin profesores*/


/*Coordinadores*/
Route::get('/controlEsc/Coordinadores', 'CoordinadorController@create')->name('FC');
Route::post('/controlEsc/Coordinadores', 'CoordinadorController@store')->name('FC');
Route::get('/controlEsc/edicionCoordinadores', 'CoordinadorController@index')->name('ECT');
Route::get('/controlEsc/editarCoordinadores{Clave}', 'CoordinadorController@edit')->name('EECT');
Route::post('/controlEsc/editarCoordinadores{Clave}', 'CoordinadorController@update')->name('EFC');
Route::get('/controlEsc/eliminarCoordinadores{Clave}', 'CoordinadorController@destroy')->name('EC');


Route::get('/coordinador/Profesores', function(){
    return view('coordinador/profesoresCoordinadores');
})->name('profesoresCoordinador');

Route::get('/coordinador/Grupo', function(){
    return view('coordinador/grupoCoordinador');
})->name('grupoCoordinador');

Route::get('/coordinador/AsignarProfesor', function(){
    return view('coordinador/asignarCoordinador');
})->name('asignarCoordinador');

/*Fin Coordinadores*/




/*Fin de ecion a tabla usuarios*/


/*Formulario de registro de Grupos y Carreras*/
Route::get('/controlEsc/carreras', function(){
    return view('controlEscolar/formCarreras');
})->name('CF');

Route::get('/controlEsc/grupos', function(){
    return view('controlEscolar/formGrupos');
})->name('GF');

/**************fin de formulario de registro de grupos y carreras*/
/*Consulta y edicion de alumnos parciales y asistencias*/
Route::get('/controlEsc/PA', function(){
    return view('controlEscolar/mParciales-Asistencias');
})->name('mPA');

Route::get('/controlEsc', function(){
    return view('controlEscolar/kardex');
})->name('Kardex');

Route::get('/controlEsc/hi', function(){
    return view('controlEscolar/historico');
})->name('hist');

Route::get('/controlEsc/plan-estudios', function(){
    return view('controlEscolar/plan-estudios');
})->name('PS');


Route::get('/controlEsc/config', function(){
    return view('controlEscolar/rolpermissions');
})->name('rs');

Route::get('/controlEsc/configfrp', function(){
    return view('controlEscolar/formRolP');
})->name('frs');

/*Coordinadores*/
Route::get('/Coordinadores', function(){
    return view('coordinador/perfilCoordinadores');
})->name('Datos');

Route::get('/Coordinadores/kardex', function(){
    return view('coordinador/kardexCoordinadores');
})->name('KA');

Route::get('/Coordinadores/plan', function(){
    return view('coordinador/planCoordinadores');
})->name('plan');

Route::get('/Coordinadores/calf', function(){
    return view('coordinador/calificacionesCoordinadores');
})->name('cal');

Route::get('/Coordinadores/adeudos', function(){
    return view('coordinador/adeudosCoordinadores');
})->name('Adeudos');

Route::get('/Coordinadores/inicio', function(){
    return view('coordinador/inicioCoordinadores');
})->name('Inicio');

Route::get('/Coordinadores/horario', function(){
    return view('coordinador/horarioCoordinadores');
})->name('Horario');


Route::get('/controlEscolar/firs', function(){
    return view('controlEscolar/formMateriasPrimerS');
})->name('AMateriaFi');

Route::get('/controlEscolar/sec', function(){
    return view('controlEscolar/formMateriasSegundoS');
})->name('AMateriaSc');

Route::get('/controlEscolar/third', function(){
    return view('controlEscolar/formMateriasTercerS');
})->name('AMateriaT');

Route::get('/controlEscolar/fouth', function(){
    return view('controlEscolar/formMateriasCuartoS');
})->name('AMateriaC');

Route::get('/controlEscolar/five', function(){
    return view('controlEscolar/formMateriasQuintoS');
})->name('AMateriaQ');


Route::get('/controlEscolar/sexto', function(){
    return view('controlEscolar/formMateriasSextoS');
})->name('AMateriaSx');

Route::get('/controlEscolar/sev', function(){
    return view('controlEscolar/formMateriasSeptimoS');
})->name('AMateriaSp');

Route::get('/controlEscolar/oct', function(){
    return view('controlEscolar/formMateriasOctavoS');
})->name('AMateriaOc');

Route::get('/controlEscolar/TPE', function(){
    return view('controlEscolar/tablaPlanEst');
})->name('STPS');

Route::get('/controlEscolar/CMa', function(){
    return view('controlEscolar/carreras-materias');
})->name('CMa');

Route::get('/controlEscolar/boletasAllp', function(){
    return view('controlEscolar/boletas');
})->name('BLA');

Route::get('/controlEscolar/Carreras', function(){
    return view('controlEscolar/tablaCarreras');
})->name('CA');

Route::get('/controlEscolar/Calif', function(){
    return view('controlEscolar/captura-calificacion-alumno');
})->name('CALIF');

Route::get('/controlEscolar/AllGr', function(){
    return view('controlEscolar/TGrupos');
})->name('AllG');
