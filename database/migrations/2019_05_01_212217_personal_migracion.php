<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonalMigracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->increments('Clave');
            $table->string('Nombre',30);
            $table->string('Ap_Paterno',45);
            $table->string('Ap_Materno',45);
            $table->string('Telefono',10);
            $table->string('Correo',45);
            $table->date('Fecha_Alta');
            $table->enum('Sexo', ['M', 'F']);   
            $table->boolean('Estatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
