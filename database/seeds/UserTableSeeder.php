<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //  factory(App\User::class, 20)->create(); //Crar 20 usuarios

  Role::create([
    'name'		=> 'Admin',
    'slug'  	=> 'slug',
    'special' 	=> 'all-access'
  ]);

  $admin = User::create([
    'name' => 'admin',
    'email' => 'admin@gmail.com',
    'password' => bcrypt('123456789')
  ]);

  $admin->assignRole('control escolar'); //nombre del rol declarado en RolesY Permisos
    }
}
