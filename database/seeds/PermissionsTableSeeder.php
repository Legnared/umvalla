<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Users
    Permission::create([
        'name'          => 'Navegar usuarios',
        'slug'          => 'users.index',//users es el modulo index es la pagina del modulo
        'description'   => 'Lista y navega todos los usuarios del sistema',
    ]);

    Permission::create([
        'name'          => 'Ver detalle de usuario',
        'slug'          => 'users.show',
        'description'   => 'Ve en detalle cada usuario del sistema',
    ]);

    Permission::create([
        'name'          => 'Edición de usuarios',
        'slug'          => 'users.edit',
        'description'   => 'Podría editar cualquier dato de un usuario del sistema',
    ]);

    Permission::create([
        'name'          => 'Eliminar usuario',
        'slug'          => 'users.destroy',
        'description'   => 'Podría eliminar cualquier usuario del sistema',
    ]);

    //Roles
    Permission::create([
        'name'          => 'Navegar roles',
        'slug'          => 'roles.index',
        'description'   => 'Lista y navega todos los roles del sistema',
    ]);

    Permission::create([
        'name'          => 'Ver detalle de un rol',
        'slug'          => 'roles.show',
        'description'   => 'Ve en detalle cada rol del sistema',
    ]);

    Permission::create([
        'name'          => 'Creación de roles',
        'slug'          => 'roles.create',
        'description'   => 'Podría crear nuevos roles en el sistema',
    ]);

    Permission::create([
        'name'          => 'Edición de roles',
        'slug'          => 'roles.edit',
        'description'   => 'Podría editar cualquier dato de un rol del sistema',
    ]);

    Permission::create([
        'name'          => 'Eliminar roles',
        'slug'          => 'roles.destroy',
        'description'   => 'Podría eliminar cualquier rol del sistema',
    ]);

    //Rol ----------------------------------ALUMNO
    Permission::create([
        'name'          => 'Navegar alumnos',///productos
        'slug'          => 'alumnos.index',//products.index
        'description'   => 'Lista y navega todos los alumnos del sistema',
    ]);

    Permission::create([
        'name'          => 'Ver detalle de un alumno',
        'slug'          => 'alumnos.show',//products.show
        'description'   => 'Ve en detalle cada alumno del sistema',
    ]);

    Permission::create([
        'name'          => 'Creación de alumnos',
        'slug'          => 'alumnos.create',
        'description'   => 'Podría crear nuevos alumnos en el sistema',
    ]);

    Permission::create([
        'name'          => 'Edición de alumnos',
        'slug'          => 'alumnos.edit',
        'description'   => 'Podría editar cualquier dato de un alumno del sistema',
    ]);

    Permission::create([
        'name'          => 'Eliminar alumnos',
        'slug'          => 'alumnos.destroy',
        'description'   => 'Podría eliminar cualquier profesor del sistema',
    ]);

    //Rol ----------------------------------PROFESOR
    Permission::create([
        'name'          => 'Navegar profesores',
        'slug'          => 'profesores.index',
        'description'   => 'Lista y navega todos los profesores del sistema',
    ]);

    Permission::create([
        'name'          => 'Ver detalle de un profesor',
        'slug'          => 'profesores.show',//products.show
        'description'   => 'Ve en detalle cada profesor del sistema',
    ]);

    Permission::create([
        'name'          => 'Creación de profesores',
        'slug'          => 'profesores.create',
        'description'   => 'Podría crear nuevos profesor en el sistema',
    ]);

    Permission::create([
        'name'          => 'Edición de profesores',
        'slug'          => 'profesores.edit',
        'description'   => 'Podría editar cualquier dato de un profesor del sistema',
    ]);

    Permission::create([
        'name'          => 'Eliminar profesores',
        'slug'          => 'profesores.destroy',
        'description'   => 'Podría eliminar cualquier profesor del sistema',
    ]);
    //Rol ----------------------------------Coordinador
    Permission::create([
        'name'          => 'Navegar coordinadores',
        'slug'          => 'coordinadores.index',
        'description'   => 'Lista y navega todos los coordinadores del sistema',
    ]);

    Permission::create([
        'name'          => 'Ver detalle de un coordinador',
        'slug'          => 'coordinadores.show',//products.show
        'description'   => 'Ve en detalle cada coordinador del sistema',
    ]);

    Permission::create([
        'name'          => 'Creación de coordinador',
        'slug'          => 'coordinadores.create',
        'description'   => 'Podría crear nuevos coordinador en el sistema',
    ]);

    Permission::create([
        'name'          => 'Edición de coordinadores',
        'slug'          => 'coordinadores.edit',
        'description'   => 'Podría editar cualquier dato de un coordinador del sistema',
    ]);

    Permission::create([
        'name'          => 'Eliminar coordinadores',
        'slug'          => 'coordinadores.destroy',
        'description'   => 'Podría eliminar cualquier coordinador del sistema',
    ]);
    }
}
