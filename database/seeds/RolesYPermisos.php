<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesYPermisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
      // Resetear la cache de  roles y permissions BERE
     app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

     // create permissions for user BERE
       Permission::create(['name' => 'create user']);
       Permission::create(['name' => 'read users']);
       Permission::create(['name' => 'update user']);
       Permission::create(['name' => 'delete user']);
       // create permissions for roles
         Permission::create(['name' => 'create role']);
         Permission::create(['name' => 'read role']);
         Permission::create(['name' => 'update role']);
         Permission::create(['name' => 'delete role']);
         // create permissions
           Permission::create(['name' => 'create permission']);
           Permission::create(['name' => 'read permissions']);
           Permission::create(['name' => 'update permission']);
           Permission::create(['name' => 'delete permission']);



       $role = Role::create(['name' => 'control escolar']);
       $role->givePermissionTo(Permission::all());




    }
}
