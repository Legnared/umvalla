<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Crypt;
use App\Alumnos;
use Illuminate\Http\Request;

class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $alumno = DB::table('alumno')
        ->join('carrera','carrera.Id_carrera','alumno.Id_carrera')
        ->select('Matricula','alumno.Nombre as na','carrera.Nombre as nc',
          'Ap_Paterno','Ap_Materno','Telefono','Correo','Sexo','alumno.Estatus',
          'Fecha_Alta','Fecha_egreso','Facebook',
          'alumno.id_carrera')
        ->get();

        /*na = nombre alumno*/
        /*nc = nombre carrera*/
        return view('controlEscolar/tablasAlumnoE',compact('alumno'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $alumno = DB::table('carrera')
      ->where('Estatus','=','1') //Para mostrar klas carreras
      ->get();
      return view('controlEscolar/formularioAlumnos',compact('alumno'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $request->validate([
    //    'matricula' => 'required|unique:alumno,matricula',
        'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
        'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'mobile' => 'required|regex:[[0-9]{7,10}]|unique:alumno,telefono',
        'facebook' => 'required|regex:[[a-zA-Z]{3,45}]',
        'email' => 'required|max:45|email|unique:alumno,correo',
        'sexo' => 'required',
        'estatus' => 'required'
      ]);

      DB::table('usuario')->insert([
      'Clave_acceso' => $request->matricula,
      'Password' => $request->password,
      'Fecha_Registro' => now(),
      'Tipo_usuario' => 4
      ]);

      DB::table('alumno')->insert([
        //'Matricula' => $request->matricula,
        'Nombre' => $request->first_name,
        'Ap_paterno' => $request->last_name,
        'Ap_materno' => $request->last_name2,
        'Telefono' => $request->mobile,
        'Correo' => $request->email,
        'Facebook' => $request->facebook,
        'Sexo' => $request->sexo,
        'Fecha_Alta' => now(),
        'Estatus' => $request->estatus,
        'Id_carrera' => $request->id_carrera
      ]);

       return back()->with('alumno','Alumno registrado correctamnete');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function show(Alumnos $alumnos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


      $id2 = Crypt::decrypt($id);
      $carrera = DB::table('carrera')
      ->where('Estatus','=','1') //Para mostrar klas carreras
      ->get();

      $alumno = DB::table('alumno')->where('Matricula','=',$id2)->get()->first();
      return view('controlEscolar.formularioAlumnosEditar', compact('alumno','carrera'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumnos  $alumnos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      DB::table('alumno')->where('Matricula','=',$id)->update([
      'Telefono' => '',
      'Correo' => ''
      ]);

      $request->validate([
    //    'matricula' => 'required|unique:alumno,matricula',
        'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
        'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'facebook' => 'required|regex:[[a-zA-Z]{3,45}]',
        'mobile' => 'required|regex:[[0-9]{7,10}]|unique:alumno,telefono',
        'email' => 'required|max:45|email|unique:alumno,correo',
        'sexo' => 'required'
      ]);

      $alumno =  DB::table('alumno')->where('Matricula','=',$id)->update([
      'Nombre'=>$request->first_name,
      'Ap_paterno' => $request->last_name,
      'Ap_materno' => $request->last_name2,
      'Facebook' => $request->facebook,
      'Correo' => $request->email,
      'Telefono' => $request->mobile,
      'Id_carrera' => $request->id_carrera,
      'Sexo' => $request->sexo
      ]);
      return back()->with('alumno','Alumno actualizado correctamente');
    }


    public function destroy($id)
    {
      $status = 0;
      $alumno =  DB::table('alumno')->where('Matricula','=',$id)->update(['Estatus'=>$status]);
      if($alumno==1){
        return back()->with('alumno','Alumno eliminado correctamente');
      }else{
        return back()->with('alumnoNo','El alumno no se pudo elimina');
      }
    }
}
