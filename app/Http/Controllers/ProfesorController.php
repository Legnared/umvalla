<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use DB;
class ProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesor = DB::table('profesor')->get();
        return view('controlEscolar.tablasProfesorE',compact('profesor')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('controlEscolar.formularioProfesores');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
        'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
        'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'mobile' => 'required|regex:[[0-9]{7,10}]|unique:profesor,telefono',
        'email' => 'required|max:45|email|unique:profesor,correo',
        'sexo' => 'required',
        'estatus' => 'required'
      ]);

      DB::table('usuario')->insert([
      'Clave_acceso' => $request->cedula,
      'Password' => $request->password,
      'Fecha_Registro' => now(),
      'Tipo_usuario' => 3
      ]);

      DB::table('profesor')->insert([
        //'Matricula' => $request->matricula,
        'Nombre' => $request->first_name,
        'Ap_paterno' => $request->last_name,
        'Ap_materno' => $request->last_name2,
        'Telefono' => $request->mobile,
        'Correo' => $request->email,
        'Cedula_profesional' => $request->cedula,
        'Sexo' => $request->sexo,
        'Fecha_ingreso' => now(),
        'Estatus' => $request->estatus
      ]);

       return back()->with('profe','Profesor registrado correctamente');
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $id2 = Crypt::decrypt($id);

      $clave = (int) $id;
      $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id2)->get()->first();

      return view('controlEscolar.formularioProfesoresEditar', compact('profesor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    DB::table('profesor')->where('Clave_profesor','=',$id)->update([
    'Telefono' => '',
    'Correo' => '']);

    $request->validate([
    //    'matricula' => 'required|unique:alumno,matricula',
    'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
    'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
    'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
    'mobile' => 'required|regex:[[0-9]{7,10}]|unique:profesor,telefono',
    'email' => 'required|max:45|email|unique:profesor,correo',
    'sexo' => 'required'
      ]);

      $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->update([
      'Nombre'=>$request->first_name,
      'Ap_paterno' => $request->last_name,
      'Ap_materno' => $request->last_name2,
      'Correo' => $request->email,
      'Telefono' => $request->mobile,
      'Sexo' => $request->sexo
      ]);
      return back()->with('profe','Profesor actualizado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $status = 0;
      $profesor =  DB::table('profesor')->where('Clave_profesor','=',$id)->update(['Estatus'=>$status]);
      if($profesor==1){
        return back()->with('profe','Profesor eliminado correctamente');
      }else{
        return back()->with('profeNo','El profesor no se pudo eliminar');
      }
    }
}
