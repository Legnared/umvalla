<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\Crypt;
use DB;
use Illuminate\Http\Request;

class CoordinadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $coordinador = DB::table('personal')->get();
      return view('controlEscolar.tablasCoordinadorE',compact('coordinador')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('controlEscolar/formularioCoordinadores') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $request->validate([
    //    'matricula' => 'required|unique:alumno,matricula',
        'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
        'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'mobile' => 'required|regex:[[0-9]{7,10}]|unique:personal,telefono',
        'email' => 'required|max:45|email|unique:personal,correo',
        'sexo' => 'required',
        'estatus' => 'required'
      ]);

      DB::table('personal')->insert([
        'Nombre' => $request->first_name,
        'Ap_paterno' => $request->last_name,
        'Ap_materno' => $request->last_name2,
        'Telefono' => $request->mobile,
        'Correo' => $request->email,
        'Sexo' => $request->sexo,
        'Estatus' => $request->estatus,
        'Fecha_Alta' => now()
      ]);

       return back()->with('coor','Coordinador registrado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  inClave * @return \Illumiate\Http\ResponClave*/
    public function edit($id)
    {
      $id2 = Crypt::decrypt($id);
      $clave = (int) $id;
      $coordinador =  DB::table('personal')->where('Clave','=',$id2)->get()->first();
      return view('controlEscolar.formularioCoordinadoresEditar', compact('coordinador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      DB::table('personal')->where('Clave','=',$id)->update([
      'Telefono' => '',
      'Correo' => '']);

      $request->validate([
    //    'matricula' => 'required|unique:alumno,matricula',
        'first_name' => 'required|min:3|max:30|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}]',
        'last_name' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'last_name2' => 'required|min:3|max:45|regex:[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}]',
        'mobile' => 'required|regex:[[0-9]{7,10}]|unique:personal,telefono',
        'email' => 'required|max:45|email|unique:personal,correo',
        'sexo' => 'required'
      ]);

      $alumno =  DB::table('personal')->where('Clave','=',$id)->update([
      'Nombre'=>$request->first_name,
      'Ap_paterno' => $request->last_name,
      'Ap_materno' => $request->last_name2,
      'Correo' => $request->email,
      'Telefono' => $request->mobile,
      'Sexo' => $request->sexo
      ]);
      return back()->with('coor','Coordinador actualizado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $status = 0;
      $profesor =  DB::table('personal')->where('Clave','=',$id)->update(['Estatus'=>$status]);
      if($profesor==1){
          return back()->with('coor','Coordinador eliminado correctamente');
      }else{
          return back()->with('coorNo','El coordinador no se pudo eliminar');
      }

    }
}
