@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Kardex :: Control Escolar
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Kardex de Boletas de alumnos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
      <div class="row">
          <div class="card-panel">
            <div class="row box-title">
              <div class="col s12">

        <div class="row">
          <div class="input-field col s12 m3">
                   <select class="icons mat_select" id="select4" name="select4">
                     <option value="" disabled selected>Carrera</option>
                     <option class="circle" value="1">Ingenieria industrial y en Sistemas organizacionales</option>
                     <option class="circle" value="2">Licenciatura en Animacion digital video juegos</option>
                     <option class="circle" value="3">Licenciatura en Arquitectura</option>
                     <option class="circle" value="4">Licenciatura en Derecho</option>
                     <option class="circle" value="5">Licenciatura en Fisioterapia y Rehabilitacion</option>
                   </select>
                 </div>

                   <div class="input-field col s3">
                    <input type="text" name="contr0l" id="searchClave"/>
                      <label>Nombre/Clave Alumno</label>
                  </div>
               </div> <br><br>
                <div class="datatable-wrapper">
                    <table class="centered responsive-table highlight bordered striped" id="mytable">
                      <center>
                      <h5>Periodo Agosto - Diciembre 2018</h5></center>
                      <thead>
                        <tr>
                          <th>Clave de alumno</th>
                          <th>Materia</th>
                          <th>Grupo</th>
                          <th>Calificacion</th>
                          <th>Asistencias</th>
                          <th>Evaluacion</th>
                          <th>Observaciones</th>
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>15120649</td>
                              <td>Ingles</td>
                              <td>A</td>
                              <td>8.9</td>
                              <td>10</td>
                              <td>Ordinaria</td>
                              <td>Ninguna</td>
                         </tr>
                         <tr>
                             <td>1512089</td>
                             <td>Matemáticas</td>
                             <td>A</td>
                             <td>8.9</td>
                             <td>10</td>
                             <td>Ordinaria</td>
                             <td>Ninguna</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  </div>
            </div>
          </div>
      </div>
@endsection
