@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Consulta :: Edicion Parciales & Asistencias
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Lista del Registro</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12"><a class="breadcrumb" href="#">Regresar a registro</a>
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('contenido')
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                  <div class="col s12">
                  </div>
                  <div class="row">
                    <div class="col s12">
                      <div class="datatable-wrapper">
                        <table class="datatable-badges display cell-border">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Paterno</th>
                              <th>Materno</th>
                              <th>Clave</th>
                              <th>Carrera</th>
                              <th>Materia</th>
                              <th>Grupo</th>
                              <th>Asistencias</th>
                              <th>Parciales</th>
                              <th>Acciones</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Angel Guillermo</td>
                              <td>Hernandez</td>
                              <td>Zambrano</td>
                              <td>15120641</td>
                              <td>Licenciatura en Animación Digital Video juegos</td>
                              <td>Busqueda y Procesamiento de Datos</td>
                              <td>A</td>
                              <td>8</td>
                              <td>8</td>
                              <td>
                                <div class="action-btns">
                                    <a class="btn-floating warning-bg" href="javascript:void(0)">
                                        <i class="material-icons">edit</i>
                                    </a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              <div class="row">
                  <br>
                  <br>
                  <br>
              </div>
            </div>
        </div>
    </div>
@endsection
