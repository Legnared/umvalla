@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Control Escolar :: Plan Estudios-Materias Semestre
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Carreras</h2>
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
    </div>
        <div class="row">
            <div class="col s12">
            <!-- Paginate	-->
            <div class="card-panel">
              <div class="row">

                <div class="col s12">
                    <table class="datatable-pagination responsive-table centered bordered striped">
                      <thead>
                        <tr>
                          <th>Clave Carrera</th>
                          <th>Carrera</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>RVOE 20130269</td>
                          <td>Licenciatura en Animación Digital Video juegos</td>
                        </tr>
                        <tr>
                          <td>RVOE 20130270</td>
                          <td>Licenciatura en Arquitectura</td>
                        </tr>
                        <tr>
                          <td>RVOE 20130271</td>
                          <td>Licenciatura en Derecho</td>
                        </tr>
                        <tr>
                          <td>RVOE 20122400</td>
                          <td>Licenciatura en Fisioterapia y Rehabilitacion</td>
                        </tr>
                        <tr>
                            <td>RVOE 20130270</td>
                            <td>Licenciatura en Mercadotecnia y Publicidad</td>
                        </tr>
                        <tr>
                            <td>RVOE 20130274</td>
                            <td>Licenciatura en Negocios Internacionales</td>
                        </tr>
                        <tr>
                            <td>RVOE 20130272</td>
                            <td>Ingeniería
                                Industrial
                                y en Sistemas
                                Organizacionales</td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="left">
                            <a class="waves-effect waves-light btn" href="{{ route('PS') }}"><i class="material-icons right">settings_backup_restore</i>Regresar a Plan Estudios</a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!--</div>-->
    <!---->

@endsection
