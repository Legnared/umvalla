@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Calificaciones
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Captura de Calificaciones Y asistencias</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row spacer">
                        <div class="col s12">
                          <div class="profile-wrapper section scrollspy">
                            <div class="profile-tabs">
                              <div class="row">
                                <div class="col s12 m12  profile-container">
                                  <ul class="tabs user-profile-tabs">
                                    <li class="tab col s4">
                                        <a href="#parcial1">Parcial 1</a>
                                    </li>
                                    <li class="tab col s4">
                                        <a href="#parcial2">Parcial 2</a>
                                    </li>
                                    <li class="tab col s4">
                                        <a href="#parcial3">Parcial 3</a>
                                    </li>
                                  </ul>
                                  <div id="parcial1">
                                    <div class="row">
                                      <div class="col s12">
                                        <div class="card-panel">
                                          <h4 class="card-title">Asistencias Y Calificaciones<i class="material-icons left">work</i>
                                          </h4>
                                          <div class="card-contents">
                                              <h1>Aqui va la calificaciones y asistencias</h1>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div id="parcial2">
                                      <div class="row">
                                        <div class="col s12">
                                          <div class="card-panel">
                                            <h4 class="card-title">Asistencias Y Calificaciones 2<i class="material-icons left">work</i>
                                            </h4>
                                            <div class="card-contents">
                                                <h1>Aqui va la calificaciones y asistencias</h1>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div id="parcial3">
                                      <div class="row">
                                        <div class="col s12">
                                          <div class="card-panel">
                                            <h4 class="card-title">Asistencias Y Calificaciones 3<i class="material-icons left">work</i>
                                            </h4>
                                            <div class="card-contents">
                                                <h1>Aqui va la calificaciones y asistencias</h1>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection
