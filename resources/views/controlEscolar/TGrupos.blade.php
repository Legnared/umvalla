@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Control Escolar :: Grupos Registrados
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2 class="center">Todos los grupos Registrados</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row center">
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">

  <div class="row">
    <div class="input-field col s12 m3">
             <select class="icons mat_select" id="select4" name="select4">
               <option value="" disabled selected>Carrera</option>
               <option class="circle" value="1">Ingenieria industrial y en Sistemas organizacionales</option>
               <option class="circle" value="2">Licenciatura en Animacion digital video juegos</option>
               <option class="circle" value="3">Licenciatura en Arquitectura</option>
               <option class="circle" value="4">Licenciatura en Derecho</option>
               <option class="circle" value="5">Licenciatura en Fisioterapia y Rehabilitacion</option>
             </select>
           </div>
           <div class="input-field col s12 m3">
                    <select class="icons mat_select" id="select4" name="select4">
                      <option value="" disabled selected>Periodo</option>
                      <option class="circle" value="1">Enero - Junio 2019</option>
                      <option class="circle" value="2">Agosto - Diciembre 2018</option>
                      <option class="circle" value="3">Enero - Junio 2018</option>
                      <option class="circle" value="4">Agosto - Diciembre 2017</option>
                      <option class="circle" value="5">Enero - Junio 2017</option>
                    </select>
                  </div>

             <div class="input-field col s12 m3">
                  <input type="text" name="contr0l" id="searchGrupo"/>
                    <label>Buscar Grupo</label>
            </div>


         </div>
         <div class="row">
             <div class="datatable-wrapper">
                 <table class="bordered striped highlight centered responsive-table" id="tablaGrupo">
                   <thead>
                     <tr>
                       <th>Grupo</th>
                       <th>Carrera</th>
                       <th>Periodo</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr>
                       <td>Lorem ipsum dolor sit amet</td>
                       <td>Lorem ipsum dolor sit amet</td>
                       <td>Lorem ipsum dolor sit amet</td>
                     </tr>
                     <tr>
                       <td>AB</td>
                       <td>Licenciatura Derecho</td>
                       <td>Agosto-Diciembre 2018</td>
                     </tr>
                   </tbody>
                 </table>
               </div>
           </div>
       </div>
      </div>
    </div>
</div>
<div class="row">
      <div class="col s12">
      </div>
      <!-- Collections-->
      <div class="col s12">
        <div class="card-panel">
          <div class="row box-title">
            <div class="col s12">
                <div class="row center">
                    <div class="row">
                          <a class="waves-effect waves-light btn" href="{{ route('GF') }}"><i class="material-icons left">replay</i>Regresar Grupos</a>
                     </div>
                </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection
