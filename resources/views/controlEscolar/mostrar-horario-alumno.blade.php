@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Horario :: Alumnos
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
            <h2>Horario de Alumnos</h2>
        </div>
    </div>
  </div>
@endsection
@section('contenido')
    <div class="col s12">
          <div class="card-panel">
            <div class="row box-title">
              <div class="col s12">
                <h5 class="content-headline">HORARIO</h5>
                <p>Periodo Actual</p>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <div class="datatable-wrapper">
                  <table class="datatable-badges display cell-border">
                    <thead>
                      <tr>
                        <th>Nombre de la Materia</th>
                        <th>Lunes</th>
                        <th>Martes</th>
                        <th>Miercoles</th>
                        <th>Jueves</th>
                        <th>Viernes</th>
                        <th>Sabado</th>
                        <th>Domingo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>Fisioterapia 1</th>
                        <th>7:00 - 8:00</th>
                        <th>7:00 - 8:00</th>
                        <th>7:00 - 8:00</th>
                        <th>7:00 - 8:00</th>
                        <th>7:00 - 8:00</th>
                        <th></th>
                        <th></th>
                      </tr>
                      <tr>
                        <th>Salud Comunitaria</th>
                        <th>8:00 - 9:00</th>
                        <th>8:00 - 9:00</th>
                        <th>8:00 - 9:00</th>
                        <th>8:00 - 9:00</th>
                        <th>8:00 - 9:00</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        <div class="row">
            <br>
            <br>
            <br>
        </div>
@endsection
