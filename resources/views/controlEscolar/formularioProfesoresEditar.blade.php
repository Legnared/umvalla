@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Formulario de Registros :: Profesores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Formulario de Edición de Profesores</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')

    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <div class="row">
              @if (session()->has('profe'))
                <script>profesorEditado();</script>
              @else
                @if($profesor->Sexo=='M')
                    Editando Profesor {{$profesor->Nombre}} {{$profesor->Ap_paterno}}
                @endif
                @if($profesor->Sexo=='F')
                    Editando Profesora {{$profesor->Nombre}} {{$profesor->Ap_paterno}}
                @endif
              @endif
                <div class="row">
                  <form class="col s12 profile-info-form" method="post" action="{{route('EFP',$profesor->Clave_profesor)}}">
                    {{csrf_field()}}
                    <div class="card-panel profile-form-cardpanel">
                      <div class="row box-title">
                        <div class="col s12">
                            <a class="waves-effect waves-light btn basic-btn right light-blue darken-4" href="{{ route('EPT') }}"><i class="material-icons left">person_add</i>Ir a tabla de Registro</a>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12"><i class="material-icons prefix">person</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="{{$profesor->Nombre}}">
                          <label for="first_name">Nombre(s)</label>
                             {{$errors->first('first_name')}}
                        </div>
                      </div>
                      <div class="row">
                          <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                            <input class="validate" type="text" id="last_name" name="last_name" required title="Formato: De 3 a 45 letras" value="{{$profesor->Ap_paterno}}" autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}">
                            <label for="last_name">Apellido Paterno</label>
                            {{$errors->first('last_name')}}
                          </div>
                          <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                              <input class="validate" type="text" id="last_name2" name="last_name2" required title="Formato: De 3 a 45 letras" value="{{$profesor->Ap_materno}}" autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}">
                            <label for="last_name2">Apellido Materno</label>
                            {{$errors->first('last_name2')}}
                          </div>
                      </div>
                      <div class="row">
                        <div class="input-field col m6 s12"><i class="material-icons prefix">vpn_key</i>
                            <input type="text" id="cedula" pattern="[A-Z]{8}" title="Formato: 8 letras"   autocomplete="off" name="cedula" required value="{{$profesor->Cedula_profesional}}" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}" title="Formato: DE 3 a 45 letras" class="validate"/>
                            <label for="cedula">Cédula Profesional</label>
                            {{$errors->first('cedula')}}
                        </div>
                          <div class="input-field col m6 s12"><i class="material-icons prefix">vpn_key</i>
                              <input type="text" autocomplete="off" value="" id="matricula" required name="matricula"  class="validate"/>
                              <label for="matricula">Clave de Acceso</label>

                          </div>

                      </div>

                      <div class="row">
                        <div class="input-field col m6 s12"><i class="material-icons prefix">email</i>
                          <input class="validate" required  type="email" id="email" autocomplete="off" name="email" value="{{$profesor->Correo}}">
                          <label for="email">Email</label>
                          {{$errors->first('email')}}
                        </div>
                        <div class="input-field col m6 s12"><i class="material-icons prefix">phone</i>
                          <input class="validate" type="text" id="mobile" name="mobile" value="{{$profesor->Telefono}}" pattern="[0-9]{7,10}" autocomplete="off" required title="Formato: De 7 a 10 números">
                          <label for="mobile">Mobile</label>
                          {{$errors->first('mobile')}}
                        </div>
                      </div>

                      <div class="row">
                         <p class="col s12">
                            <i class="material-icons prefix">contacts</i><label for="">Sexo</label>
                         </p>
                          <div class="input-group radio">
                            @if($profesor->Sexo=='M')
                              <p class="col m6 s12">
                                  <input type="radio" checked required title="Seleccione una opcion" name="sexo" id="sexom" value="M">
                                  <label for="sexom" >Masculino</label>
                              </p>
                               <p class="col m6 s12">
                                 <input type="radio" name="sexo" id="sexof" value="F">
                                 <label for="sexof">Femenino</label>
                               </p>
                             @endif
                             @if($profesor->Sexo=='F')
                               <p class="col m6 s12">
                                   <input type="radio"  required title="Seleccione una opcion" name="sexo" id="sexom" value="M">
                                   <label for="sexom" >Masculino</label>
                               </p>
                                <p class="col m6 s12">
                                  <input type="radio" checked name="sexo" id="sexof" value="F">
                                  <label for="sexof">Femenino</label>
                                </p>
                              @endif
                          </div>

                      </div>
                      <input type="hidden" name="estatus" id="estatus" value="1">


                      <div class="row">
                        <div class="input-field col s12 right-align">
                          <button class="btn waves-effect waves-set" type="submit" name="update_profile">Registrar<i class="material-icons right">save</i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
            </br>
            </br>
            </br>
            </div>
          </form>
      </div>
@endsection
