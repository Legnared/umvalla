@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Historico :: Control Escolar
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Historico de alumnos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">

  <div class="row">
    <div class="input-field col s12 m3">
             <select class="icons mat_select" id="select4" name="select4">
               <option value="" disabled selected>Carrera</option>
               <option class="circle" value="1">Ingenieria industrial y en Sistemas organizacionales</option>
               <option class="circle" value="2">Licenciatura en Animacion digital video juegos</option>
               <option class="circle" value="3">Licenciatura en Arquitectura</option>
               <option class="circle" value="4">Licenciatura en Derecho</option>
               <option class="circle" value="5">Licenciatura en Fisioterapia y Rehabilitacion</option>
             </select>
           </div>
           <div class="input-field col s12 m3">
                    <select class="icons mat_select" id="select4" name="select4">
                      <option value="" disabled selected>Periodo</option>
                      <option class="circle" value="1">Enero - Junio 2019</option>
                      <option class="circle" value="2">Agosto - Diciembre 2018</option>
                      <option class="circle" value="3">Enero - Junio 2018</option>
                      <option class="circle" value="4">Agosto - Diciembre 2017</option>
                      <option class="circle" value="5">Enero - Junio 2017</option>
                    </select>
                  </div>

             <div class="input-field col s3">
              <input type="text" name="contr0l" id="searchClave"/>
                <label>Nombre/Clave Alumno</label>
            </div>
            <br>
           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
           <a class="waves-effect waves-light btn">Buscar</a>
         </div> <br><br>
          <div class="datatable-wrapper">
              <table class="datatable-badges display cell-border">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Paterno</th>
                    <th>Materia</th>
                    <th>Grupo</th>
                    <th>Calificacion</th>
                    <th>Asistencias</th>
                    <th>Evaluacion</th>
                    <th>Observaciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jorge Armando</td>
                    <td>Rodriguez Herrera</td>
                    <td>Ingles</td>
                    <td>A</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
      </div>
    </div>
</div>
@endsection
