@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Contro Escolar :: Roles & Permisos
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Lista de Roles y Permisos</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12">
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('contenido')
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                  <div class="col s12">
                  </div>
                  <div class="row">
                      <div class="right">
                          <p><a class="waves-effect waves-light btn" href="{{ route('frs') }}" title="Agregar Roles y Permisos"><i class="material-icons right">library_add</i></a></p>
                      </div>
                    <div class="col s12">
                      <div class="datatable-wrapper responsive-table">
                        <table class="datatable-badges display cell-border">
                          <thead>
                            <tr>
                              <th>Rol</th>
                              <th>Descripción</th>
                              <th>Permisos</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Admin</td>
                              <td>Control Escolar</td>
                              <td>Editar Registros</td>
                              <td>
                                <div class="action-btns">
                                    <a class="btn-floating warning-bg" href="javascript:void(0)" title="Editar"><i class="material-icons">edit</i>
                                    </a>
                                    <a class="btn-floating error-bg" href="javascript:void(0)" title="Eliminar"><i class="material-icons">delete</i>
                                    </a>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              <div class="row">
                  <br>
                  <br>
                  <br>
              </div>
            </div>
        </div>
    </div>
@endsection
