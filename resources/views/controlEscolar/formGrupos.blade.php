@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Registros :: Grupos
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Formulario de Registro Grupos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <div class="row">
                <div class="row">
                  <form class="col s12 profile-info-form" method="post" action="#">
                    <div class="card-panel profile-form-cardpanel">
                      <div class="row box-title">
                      </div>
                      <div class="row">
                        <div class="input-field col s12"><i class="material-icons prefix">group_work</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Nombre de Grupo</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12 right-align">
                          <button class="btn waves-effect waves-set" href="#" type="submit" name="update_profile">Registrar Grupo<i class="material-icons right">save</i>
                          </button>
                        </div>
                        <p class="left"><a class="waves-effect waves-light btn" href="{{ route('AllG') }}"><i class="material-icons right">toc</i>Ver Grupos</a></p>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
            </br>
            </br>
            </br>
            </div>
          </form>
      </div>
@endsection
