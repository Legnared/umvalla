@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Control Escolar :: Acceso Rapido
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Bienvenido</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">
            <div class="row">
                <div class="row" style="margin-top:10px !important;">
                    <div class="row">
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/estudiante.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todos los Alumnos inscritos
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('EAT')}}"><i class="material-icons left">person_add</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevos alumnos a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div>
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/aula.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todos los Profesores inscritos
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('EPT')}}"><i class="material-icons left">person_add</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevos profesores a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div>
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/coordinacion.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todos los coordinadores inscritos
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('ECT')}}"><i class="material-icons left">person_add</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevos coordinadores a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div>
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/test.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todas las carreras inscritas
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('CA')}}"><i class="material-icons left">trending_flat</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevas carreras a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div>
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/briefcase.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todos las grupos registrados
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('AllG')}}"><i class="material-icons left">trending_flat</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevas grupos a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div>
                        <div class="col s12 m12 l4 user-widget z-depth-1">
                          <div class="card blue-grey darken-1 user-widget-type3">
                            <div class="card-image"><img class="responsive-img" src="../img/svg/diploma.svg" alt="placeholder">
                            </div>
                            <div class="card-content">
                              <p>Aquí se muestran todos las calificaciones registrados
                                  en el sistema.</p>
                            </div>
                            <div class="card-action"><a class="waves-effect waves-light btn basic-btn" href="{{route ('CALIF')}}"><i class="material-icons left">trending_flat</i>Ir</a><a class="activator waves-effect waves-light btn-floating no-bg btn-flat right" href="javascript:void(0)"><i class="material-icons grey-text text-darken-4" title="Ver">more_vert</i></a></div>
                            <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ayuda<i class="material-icons right">close</i></span>
                              <p>Pudes ver más acciones como edición, elimnación y agregar nuevas calificaciones a el sistema, si ingresas en el botón de "Ir".</p>
                            </div>
                          </div>
                        </div></div>
              </div>
        </div>
      </div>
    </div>
@endsection
