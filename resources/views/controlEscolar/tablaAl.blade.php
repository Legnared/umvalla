@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Tabla de Calificaciones
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Registro de Calificaciones y Asistencias</h2>
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
            <div class="col s12">
                <h4 class="content-headline"> Resultado de Calificaciones Parciales y Asistencias</h4>
           <p>Se mostraran cada uno de los resultados de los parciales con respectivas asistencias a lo largo del semestre para cada una de las materias respecto a cada licenciatura de la Universidad Marista Valladolid.</p>
           <div class="right">
               <a class="waves-effect waves-light btn"><i class="material-icons right">group_work</i>Parciales</a>
               <a class="waves-effect waves-light btn"><i class="material-icons right">perm_contact_calendar</i>Asistencias</a>
           </div>

            </div>
    </div>
        <div class="row">
            <div class="col s12">
            <!-- Paginate	-->
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <div class="datatable-wrapper">
                    <table class="datatable-pagination mat-datatable display cell-border">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Clave</th>
                          <th>Parcial 1</th>
                          <th>Parcial 2</th>
                          <th>Parcial 3</th>
                          <th>Porcentaje de faltas</th>
                          <th>Final</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Andrew</td>
                          <td>15120641</td>
                          <td>10</td>
                          <td>9.5</td>
                          <td>7.5</td>
                          <td>85</td>
                          <td>8.2</td>
                        </tr>
                        <tr>
                          <td>Carolina</td>
                          <td>15120641</td>
                          <td>10</td>
                          <td>9.5</td>
                          <td>7.5</td>
                          <td>85</td>
                          <td>8.2</td>
                        </tr>
                        <tr>
                          <td>Angel</td>
                          <td>15120641</td>
                          <td>10</td>
                          <td>9.5</td>
                          <td>7.5</td>
                          <td>85</td>
                          <td>8.2</td>
                        </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!--</div>-->
    <!---->

@endsection
