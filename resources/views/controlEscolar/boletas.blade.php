@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Kardex :: Control Escolar
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Boletas de alumnos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')

    <div class="row">
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row center">
                        <div class="input-field col s8">
                            <select class="icons mat_select" id="select4" name="select4">
                                <option value="" disabled selected>Periodo</option>
                                <option class="circle" value="1">Enero - Junio 2019</option>
                                <option class="circle" value="2">Agosto - Diciembre 2018</option>
                                <option class="circle" value="3">Enero - Junio 2018</option>
                                <option class="circle" value="4">Agosto - Diciembre 2017</option>
                                <option class="circle" value="5">Enero - Junio 2017</option>
                             </select>

                      </div>
                      <br>
                      <a class="waves-effect waves-light btn">Buscar</a>
                    </div>
                    
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection
