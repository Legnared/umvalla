@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Consultas :: Edición Alumnos
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Lista del Registro</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12"><a class="breadcrumb" href="{{ route('FA') }}">Regresar a registro</a>
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('contenido')
          <div class="col s12">
            @if (session()->has('alumno'))
              <script>
                alumnoEliminado();
              </script>
            @endif
            @if (session()->has('alumnoNo'))
              <script>
     alumnoNoEliminado(); //Llamar página de error
              </script>
            @endif
            <div class="card-panel">
              <div class="row box-title">
                  <div class="col s12">
              </div>

                  <div class="row">
                    <div class="col s12">
                      <div class="datatable-wrapper">
                        <table class="datatable-badges display cell-border">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Paterno</th>
                              <th>Materno</th>
                              <th>Telefono</th>
                              <th>E-mail</th>
                              <th>Facebook</th>
                              <th>Sexo</th>
                              <th>Carrera</th>
                              <th>Fecha ingreso</th>

                            </tr>
                          </thead>
                          <tbody>
                            @if($alumno->count())
                            @foreach($alumno as $a)
                            @if($a->Estatus==1)
                            <tr>
                              <td>{{$a->na}}</td>
                              <td>{{$a->Ap_Paterno}}</td>
                              <td>{{$a->Ap_Materno}}</td>
                              <td>{{$a->Telefono}}</td>
                              <td>{{$a->Correo}}</td>
                              <td>{{$a->Facebook}}</td>
                              <td>{{$a->Sexo}}</td>
                              <td>{{$a->nc}}</td>
                              <td>{{$a->Fecha_Alta}}</td>
                              <td>
                                <div class="action-btns">
                                  <a class="btn-floating warning-bg" title="Editar" href="{{route('EEAT',['Matricula'=>Crypt::encrypt($a->Matricula)])}}"><i class="material-icons">edit</i></a>
                                  <a class="btn-floating info-bg"  href="javascript:void(0)"><i class="material-icons">import_export</i></a>
                                  <a class="btn-floating error-bg" title="Eliminar" href="{{route('EA',$a->Matricula)}}"><i class="material-icons">delete</i></a></div>

                              </td>
                            </tr>
                            @endif
                            @endforeach
                            @else
                            @endif
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
              <div class="row">
                  <br>
                  <br>
                  <br>
              </div>
            </div>
        </div>
    </div>
@endsection
