@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Captura de Parciales
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Captura de Calificaciones Y asistencias</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="col s12">
                          <div class="card-panel">
                            <div class="row box-title">
                              <div class="col s12">
                                <h5 class="content-headline">Tabla de Acciones</h5>
                                <p>Captura</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <div class="datatable-wrapper">
                                  <table class="datatable-badges display cell-border">
                                    <thead>
                                      <tr>
                                        <th>Nombre</th>
                                        <th>Paterno</th>
                                        <th>Materno</th>
                                        <th>Clave</th>
                                        <th>Calificaciones</th>
                                        <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>Angel Guillermo</td>
                                        <td>Hernandez</td>
                                        <td>Zambrano</td>
                                        <td>15120641</td>
                                        <td>7.0</td>
                                        <td>
                                          <div class="action-btns"><a class="btn-floating warning-bg" href="javascript:void(0)"><i class="material-icons">edit</i></a><a class="btn-floating info-bg" href="javascript:void(0)"><i class="material-icons">import_export</i></a><a class="btn-floating error-bg" href="javascript:void(0)"><i class="material-icons">delete</i></a></div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                        <div class="row">
                            <br>
                            <br>
                            <br>
                        </div>
                </div>
            </div>
              </div>
          </div>
      </div>
@endsection
