@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Lista :: Alumnos
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Inicio</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12"><a class="breadcrumb" href="#">Inicio</a>
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('content')
    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>Column 1</th>
                        <th>Column 2</th>
                        <th>Column 3</th>
                        <th>Column 4</th>
                        <th>Column 5</th>
                        <th>Column 6</th>
                        <th>Column 7</th>
                        <th>Column 8</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Row 1 Data 1</td>
                        <td>Row 1 Data 2</td>
                        <td>Row 1 Data 3</td>
                        <td>Row 1 Data 4</td>
                        <td>Row 1 Data 5</td>
                        <td>Row 1 Data 6</td>
                        <td>Row 1 Data 7</td>
                        <td>Row 1 Data 8</td>
                    </tr>
                    <tr>
                        <td>Row 2 Data 1</td>
                        <td>Row 2 Data 2</td>
                        <td>Row 2 Data 3</td>
                        <td>Row 2 Data 4</td>
                        <td>Row 2 Data 5</td>
                        <td>Row 2 Data 6</td>
                        <td>Row 2 Data 7</td>
                        <td>Row 2 Data 8</td>
                    </tr>
                    <tr>
                        <td>Row 3 Data 1</td>
                        <td>Row 3 Data 2</td>
                        <td>Row 3 Data 3</td>
                        <td>Row 3 Data 4</td>
                        <td>Row 3 Data 5</td>
                        <td>Row 3 Data 6</td>
                        <td>Row 3 Data 7</td>
                        <td>Row 3 Data 8</td>
                    </tr>
                    <tr>
                        <td>Row 4 Data 1</td>
                        <td>Row 4 Data 2</td>
                        <td>Row 4 Data 3</td>
                        <td>Row 4 Data 4</td>
                        <td>Row 4 Data 5</td>
                        <td>Row 4 Data 6</td>
                        <td>Row 4 Data 7</td>
                        <td>Row 4 Data 8</td>
                    </tr>
                    <tr>
                        <td>Row 5 Data 1</td>
                        <td>Row 5 Data 2</td>
                        <td>Row 5 Data 3</td>
                        <td>Row 5 Data 4</td>
                        <td>Row 5 Data 5</td>
                        <td>Row 5 Data 6</td>
                        <td>Row 5 Data 7</td>
                        <td>Row 5 Data 8</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
