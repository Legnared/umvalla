@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Consultas :: Edición Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Lista del Registro</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12"><a class="breadcrumb" href="{{ route('FC') }}">Edicion de Usuario</a>
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('contenido')
    <div class="col s12">
      @if (session()->has('coor'))
        <script>
          coordinadorEliminado();
        </script> <!--Darle estilo a este cuadro, que aparece despues de eliminar un registro correcto-->
      @endif
      @if (session()->has('coorNO'))
        <script type="text/javascript">
          coordinadorNoEliminado();
        </script>
      @endif
          <div class="card-panel">
            <div class="row box-title">
              <div class="col s12">
                <h5 class="content-headline">Tabla de Edición</h5>
                <p>Usuarios Coordinadores</p>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <div class="datatable-wrapper">
                  <table class="datatable-badges display cell-border">
                    <thead>
                      <tr>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Sexo</th>
                        <th>Fecha</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($coordinador->count())
                      @foreach($coordinador as $c)
                      @if($c->Estatus==1)
                      <tr>
                        <td>{{$c->Clave}}</td>
                        <td>{{$c->Nombre}}</td>
                        <td>{{$c->Ap_Paterno}}</td>
                        <td>{{$c->Ap_Materno}}</td>
                        <td>{{$c->Telefono}}</td>
                        <td>{{$c->Correo}}</td>
                        <td>{{$c->Sexo}}</td>
                        <td>{{$c->Fecha_Alta}}</td>
                        <td>
                          <div class="action-btns">
                            <a class="btn-floating warning-bg" title="Editar" href="{{route('EECT',['Clave_profesor'=>Crypt::encrypt($c->Clave)])}}"><i class="material-icons">edit</i></a>
                            <a class="btn-floating info-bg"  href="javascript:void(0)"><i class="material-icons">import_export</i></a>
                            <a class="btn-floating error-bg" title="Eliminar" href="{{route('EC',$c->Clave)}}"><i class="material-icons">delete</i></a></div>
                          </div>
                        </td>
                      </tr>
                      @endif
                      @endforeach
                      @else
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        <div class="row">

            <br>
            <br>
            <br>
        </div>
@endsection
