@extends('layouts.plantilla-Control-Esc')
@section('titulo')
        Consultas :: Edición Profesores
@endsection
@section('titulos-cabezera')
    <div class="main-header">
      <div class="sec-page">
        <div class="page-title">
          <h2>Lista del Registro</h2>
        </div>
      </div>
      <div class="sec-breadcrumb">
        <nav class="breadcrumbs-nav left">
          <div class="nav-wrapper">
            <div class="col s12"><a class="breadcrumb" href="{{ route('FP') }}">Edicion de Usuario</a>
            </div>
          </div>
        </nav>
      </div>
  </div>
@endsection
@section('contenido')
    <div class="col s12">
      @if (session()->has('profe'))
        <script type="text/javascript">
          profesorEliminado();
        </script><!--Darle estilo a este cuadro, que aparece despues de eliminar un registro correcto-->
      @endif
      @if (session()->has('profeNo'))
        <script type="text/javascript">
          profesorNoEliminado();//vista error
        </script>
      @endif
          <div class="card-panel">
            <div class="row box-title">
              <div class="col s12">
                <h5 class="content-headline">Tabla de Edición</h5>
                <p>Usuarios Profesores</p>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <div class="datatable-wrapper">
                  <table class="datatable-badges display cell-border">
                    <thead>
                      <tr>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Paterno</th>
                        <th>Materno</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Sexo</th>
                        <th>Cédula</th>
                        <th>Fecha Ingreso</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($profesor->count())
                      @foreach($profesor as $p)
                      @if($p->Estatus==1)
                      <tr>
                        <td>{{$p->Clave_profesor}}</td>
                        <td>{{$p->Nombre}}</td>
                        <td>{{$p->Ap_paterno}}</td>
                        <td>{{$p->Ap_materno}}</td>
                        <td>{{$p->Telefono}}</td>
                        <td>{{$p->Correo}}</td>
                        <td>{{$p->Sexo}}</td>
                        <td>{{$p->Cedula_profesional}}</td>
                        <td>{{$p->Fecha_ingreso}}</td>


                        <td>
                          <div class="action-btns">
                            <a class="btn-floating warning-bg" title="Editar" href="{{route('EEPT',['Clave_profesor'=>Crypt::encrypt($p->Clave_profesor)])}}"><i class="material-icons">edit</i></a>
                            <a class="btn-floating info-bg"  href="javascript:void(0)"><i class="material-icons">import_export</i></a>
                            <a class="btn-floating error-bg" title="Eliminar" href="{{route('EP',$p->Clave_profesor)}}"><i class="material-icons">delete</i></a></div>
                          </div>
                        </td>
                      </tr>
                      @endif
                      @endforeach
                      @else
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        <div class="row">

            <br>
            <br>
            <br>
        </div>
@endsection
