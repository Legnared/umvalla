@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Configuracion::Control Escolar-Role & Permisos
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Formulario Roles & Permisos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <div class="row">
                <div class="row">
                  <form class="col s12 profile-info-form" method="post" action="#">
                    <div class="card-panel profile-form-cardpanel">
                      <div class="row box-title">
                      </div>
                      <div class="row">
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Rol</label>
                        </div>
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Descripción</label>
                        </div>
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Permisos</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12 right-align">
                          <button class="btn waves-effect waves-set" type="submit" name="update_profile">Registrar Carrera<i class="material-icons right">save</i>
                          </button>
                          <div class="left">
                              <p><a class="waves-effect waves-light btn" href="{{ route('rs') }}" title="Regresar a Tabla de Roles & permisos"><i class="material-icons right">repeat</i>Regresar</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
            </br>
            </br>
            </br>
            </div>
          </form>
      </div>
@endsection
