@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Plan Estudios-Carreras & Materias :: Control Escolar
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Seccion de Plan Estudio</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
      <div class="row">
                  <!-- Popout-->
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                  <h5>Carreras & Materias</h5>
                  <p>Creación de  carreras y materias respecto Plan de estudios de la Universidad Marista Valladolid.</p>
                </div>
              </div>
              <div class="row">
                  <div class="row">
                      <div class="col s12">
                          <form class="col s12 profile-info-form" method="post" action="#">
                            <div class="card-panel profile-form-cardpanel">
                              <div class="row box-title">
                              </div>
                              <div class="row col s12">
                                  <div class="input-field col s12 m6">
                                           <select class="icons mat_select" id="plan" name="plan">
                                             <option value="" disabled selected>Periodo Plan Estudios</option>
                                             <option class="circle" value="1">Enero - Junio 2019</option>
                                             <option class="circle" value="2">Agosto - Diciembre 2018</option>
                                             <option class="circle" value="3">Enero - Junio 2018</option>
                                             <option class="circle" value="4">Agosto - Diciembre 2017</option>
                                             <option class="circle" value="5">Enero - Junio 2017</option>
                                           </select>
                                         </div>
                                <div class="input-field col s12 m6"><i class="material-icons prefix">subtitles</i>
                                  <input class="validate" type="text" id="first_name" name="first_name"   pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                                  <label for="first-name">Nombre de la Carrera</label>
                                </div>
                              </div>
                              <div class="row">

                                <div class="input-field col s12 right-align">
                                  <button class="btn waves-effect waves-set" type="submit" name="update_profile">Registrar Carrera<i class="material-icons right">save</i>
                                  </button>
                                  <a class="waves-effect waves-light btn" href="{{ route('PS') }}">Regresar<i class="material-icons left">settings_backup_restore</i></a>
                                </div>
                              </div>
                            </div>
                          </form>
                      </div>
                  </div>
                <div class="col s12">
                    <div class="input-field col s12 m6">
                    </div>
                    <div class="input-field col s12 m6">
                     </div>
                     <div class="col s12">
                         <div class="row">
                             <div class="col s12 m6">
                                 <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                   <li class="active">
                                     <div class="collapsible-header active"><i class="material-icons">subtitles</i>1 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center">
                                           <a class="waves-effect waves-light btn" href="{{ route('AMateriaFi') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a>
                                       </p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>2 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaSc') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>3 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaT') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>4 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaC') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                 </ul>
                             </div>
                             <div class="col s12 m6">
                                 <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>5 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center">
                                           <a class="waves-effect waves-light btn" href="{{ route('AMateriaQ') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a>
                                       </p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>6 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaSx') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>7 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaSp') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                   <li>
                                     <div class="collapsible-header"><i class="material-icons">subtitles</i>8 Semestre</div>
                                     <div class="collapsible-body">
                                       <p class="center"><a class="waves-effect waves-light btn" href="{{ route('AMateriaOc') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestre</a></p>
                                     </div>
                                   </li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                </div>
              </div>
            </div>
        </div>
@endsection
