@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Plan Estudios :: Control Escolar
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Seccion de Plan Estudio</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
      <div class="row">
                  <!-- Popout-->
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                  <h5>Plan de estudios</h5>
                  <p>Creación de plan de estudios de la Universidad Marista Valladolid.</p>
                </div>
              </div>
              <div class="row">
                  <div class="row">
                      <div class="col s12">
                          <div class="row">
                              <div class="row">
                                <form class="col s12 profile-info-form" method="post" action="#">
                            <div class="card-panel profile-form-cardpanel ">
                                    <div class="row box-title">
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12 m6"><i class="material-icons prefix">assignment</i>
                                          <input class="validate" type="text" id="first_date" name="first_date"  autocomplete="off"  pattern="^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$" required title="Formato: De 4 Numero" value="">
                                          <label for="first-date">Inicio Plan Estudios</label>
                                        </div>
                                        <div class="input-field col s12 m6"><i class="material-icons prefix">assignment</i>
                                          <input class="validate" type="text" id="end_date" name="end_date"  autocomplete="off"  pattern="^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$" required title="Formato: De 4 Numero" value="">
                                          <label for="end-date">Fin Plan de Estudios</label>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                    <div class="row">
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="input-field col s12 m3 right">
                                                  <button class="btn waves-effect waves-set" type="submit" name="update_profile">+ Periodo<i class="material-icons left">save</i>
                                                  </button>
                                                </div>
                                                <div class="input-field col s12 m3 left">
                                                  <a class="waves-effect waves-light btn" href="{{ route('STPS') }}"> Materias
                                                      <i class="material-icons right">view_headline</i>
                                                  </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                          </div>
                      </div>
                  </div>
                <div class="col s12">
                  <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                    <li class="active">
                      <div class="collapsible-header active"><i class="material-icons">subtitles</i>Agregar Plan de Estudios</div>
                      <div class="collapsible-body">
                        <p class="center"><a class="waves-effect waves-light btn" href="{{ route('CMa') }}"><i class="material-icons right">input</i>Agregar Materias a plan de estudio respecto a Semestres</a></p>
                      </div>
                    </li>
                    <li>
                      <div class="collapsible-header"><i class="material-icons">subtitles</i>Ver Plan de Estudios</div>
                      <div class="collapsible-body">
                        <p class="center"><a class="waves-effect waves-light btn" href="{{ route('STPS') }}"><i class="material-icons right">input</i>Ver todo Plan estudios</a></p>
                      </div>
                    </li>

                  </ul>
                </div>
              </div>
            </div>
        </div>
@endsection
