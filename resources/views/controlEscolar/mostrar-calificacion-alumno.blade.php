@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Control Escolar :: Calificacion Final
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Tabla de Calificaciones</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">
            <div class="row">
                <div class="row" style="margin-top:10px !important;">
                    <div class="col s12">
                          <div class="card-panel">
                            <div class="row box-title">
                              <div class="col s12">
                                <h5 class="content-headline">CALIFICACIONES</h5>
                                <p>Periodo Actual</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <div class="datatable-wrapper">
                                  <table class="datatable-badges display cell-border">
                                    <thead>
                                      <tr>
                                        <th>Nombre de la Materia</th>
                                        <th>No. Creditos</th>
                                        <th>Periodo 1</th>
                                        <th>Periodo 2</th>
                                        <th>Periodo 3</th>
                                        <th>Calificacion Final</th>
                                        <th>Faltas</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <th>Fisioterapia 1</th>
                                        <th>5</th>
                                        <th>80</th>
                                        <th>70</th>
                                        <th></th>
                                        <th></th>
                                        <th>2</th>
                                      </tr>
                                      <tr>
                                        <th>Salud Comunitaria</th>
                                        <th>4</th>
                                        <th>85</th>
                                        <th>90</th>
                                        <th></th>
                                        <th></th>
                                        <th>2</th>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
