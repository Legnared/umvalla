@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Formulario de Registros :: Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Formulario de Edición de Coordinadores</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <div class="row">
              @if (session()->has('coor'))
                  <script>coordinadorEditado();</script>  <!--Darle estilo a este cuadro, que aparece despues de un registro correcto-->
              @else
                @if($coordinador->Sexo=='M')
                  Editando Coordinador {{$coordinador->Nombre}} {{$coordinador->Ap_Paterno}}
                @endif
                @if($coordinador->Sexo=='F')
                  Editando Coordinadora {{$coordinador->Nombre}} {{$coordinador->Ap_Paterno}}
                @endif
              @endif
                <div class="row">
                  <form class="col s12 profile-info-form" method="post" action="{{route('EFC',$coordinador->Clave)}}">
                    {{csrf_field()}}
                    <div class="card-panel profile-form-cardpanel">
                      <div class="row box-title">
                        <div class="col s12">
                            <a class="waves-effect waves-light btn basic-btn right light-blue darken-4" href="{{ route('ECT') }}"><i class="material-icons left">person_add</i>Ir a tabla de Registro</a>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12"><i class="material-icons prefix">person</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="{{$coordinador->Nombre}}">
                          <label for="first_name">Nombre(s)</label>
                             {{$errors->first('first_name')}}
                        </div>
                      </div>
                      <div class="row">
                          <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                            <input class="validate" type="text" id="last_name" name="last_name" required title="Formato: De 3 a 45 letras" value="{{$coordinador->Ap_Paterno}}" autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}">
                            <label for="last_name">Apellido Paterno</label>
                            {{$errors->first('last_name')}}
                          </div>
                          <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                              <input class="validate" type="text" id="last_name2" name="last_name2" required title="Formato: De 3 a 45 letras" value="{{$coordinador->Ap_Materno}}" autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,45}">
                            <label for="last_name2">Apellido Materno</label>
                            {{$errors->first('last_name2')}}
                          </div>
                      </div>
                      <div class="row">

                          <div class="input-field col m6 s12"><i class="material-icons prefix">vpn_key</i>
                              <input type="text" id="matricula" required name="matricula" autocomplete="off"  class="validate"/>
                              <label for="matricula">Clave de Acceso</label>
                          </div>

                      </div>

                      <div class="row">
                        <div class="input-field col m6 s12"><i class="material-icons prefix">email</i>
                          <input class="validate" required  type="email" id="email" autocomplete="off" name="email" value="{{$coordinador->Correo}}">
                          <label for="email">Email</label>
                          {{$errors->first('email')}}
                        </div>
                        <div class="input-field col m6 s12"><i class="material-icons prefix">phone</i>
                          <input class="validate" type="text" id="mobile" name="mobile" value="{{$coordinador->Telefono}}" pattern="[0-9]{7,10}" autocomplete="off" required title="Formato: De 7 a 10 números">
                          <label for="mobile">Mobile</label>
                          {{$errors->first('mobile')}}
                        </div>
                      </div>
                      <div class="row">
                         <p class="col s12">
                            <i class="material-icons prefix">contacts</i><label for="">Sexo</label>
                         </p>
                          <div class="input-group radio">
                            @if($coordinador->Sexo=='M')
                              <p class="col m6 s12">
                                  <input type="radio" checked required title="Seleccione una opcion" name="sexo" id="sexom" value="M">
                                  <label for="sexom" >Masculino</label>
                              </p>
                               <p class="col m6 s12">
                                 <input type="radio" name="sexo" id="sexof" value="F">
                                 <label for="sexof">Femenino</label>
                               </p>
                             @endif
                             @if($coordinador->Sexo=='F')
                               <p class="col m6 s12">
                                   <input type="radio"  required title="Seleccione una opcion" name="sexo" id="sexom" value="M">
                                   <label for="sexom" >Masculino</label>
                               </p>
                                <p class="col m6 s12">
                                  <input type="radio" checked name="sexo" id="sexof" value="F">
                                  <label for="sexof">Femenino</label>
                                </p>
                              @endif
                          </div>

                      </div>
                      <input type="hidden" name="estatus" id="estatus" value="1">


                      <div class="row">
                        <div class="input-field col s12 right-align">
                          <button class="btn waves-effect waves-set" type="submit" name="update_profile">Registrar<i class="material-icons right">save</i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
            </br>
            </br>
            </br>
            </div>
          </form>
      </div>
@endsection
