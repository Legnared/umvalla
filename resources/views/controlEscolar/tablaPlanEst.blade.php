@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Control Escolar :: Plan Estudios-Materias Semestre
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Tabla de Plan Estudios</h2>
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
            <div class="col s12">
                <h4 class="content-headline">Plan de Estudios de Registrados</h4>
            </div>
    </div>
        <div class="row">
            <div class="col s12">
            <!-- Paginate	-->
            <div class="card-panel">
              <div class="row">

                <div class="col s12">
                    <table class="datatable-pagination responsive-table centered bordered striped">
                      <thead>
                        <tr>
                          <th>Clave Carrera</th>
                          <th>Clave Materia</th>
                          <th>Carrera</th>
                          <th>Materia</th>
                          <th>Semestre</th>
                          <th>Periodo</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>LADVJP12</td>
                          <td>C12D</td>
                          <td>Licenciatura en Animación Digital Video juegos</td>
                          <td>Calculo Diferencial</td>
                          <td>1</td>
                          <td>Enero - Junio 2019</td>
                        </tr>
                        <tr>
                            <td>LFCP12</td>
                            <td>C12DD</td>
                            <td>Licenciatura en Formación Catequética</td>
                            <td>Calculo Diferencial</td>
                            <td>1</td>
                            <td>Enero - Junio 2019</td>
                        </tr>
                        <tr>
                            <td>IIESO12</td>
                            <td>C12DKL</td>
                            <td>Ingeniería
                                Industrial
                                y en Sistemas
                                Organizacionales</td>
                            <td>Calculo Diferencial</td>
                            <td>1</td>
                            <td>Enero - Junio 2019</td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="left">
                            <a class="waves-effect waves-light btn" href="{{ route('PS') }}"><i class="material-icons right">settings_backup_restore</i>Regresar a Plan Estudios</a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!--</div>-->
    <!---->

@endsection
