@extends('layouts.plantilla-Control-Esc')
@section('titulo')
    Registros de Materias :: Sexto Semestre
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Registro de Materias</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
        <div class="row" style="margin-top:10px !important;">
            <div class="row">
                <div class="row">
                  <form class="col s12 profile-info-form" method="post" action="#">
                    <div class="card-panel profile-form-cardpanel">
                      <div class="row box-title">
                      </div>
                      <div class="row">
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Clave de REVOE</label>
                        </div>
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Nombre de la Materia</label>
                        </div>
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Número Creditos</label>
                        </div>
                        <div class="input-field col s12"><i class="material-icons prefix">subtitles</i>
                          <input class="validate" type="text" id="first_name" name="first_name"  autocomplete="off"  pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{3,30}" required title="Formato: De 3 a 30 letras" value="">
                          <label for="first-name">Número de Horas</label>
                        </div>
                      </div>
                      <div class="row">
                          <div class="row">
                              <div class="col s12">
                                  <div class="input-field col s12 m3 right">
                                    <button class="btn waves-effect waves-set" type="submit" name="update_profile">Add Materia<i class="material-icons left">save</i>
                                    </button>
                                  </div>
                              </div>
                          </div>
                          <div class="col s12">
                              <div class="input-field col s12 m3 left">
                                <a class="waves-effect waves-light btn" href="{{ route('STPS') }}">Ver Materias
                                    <i class="material-icons right">view_headline</i>
                                </a>
                              </div>
                          </div>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
            </br>
            </br>
            </br>
            </div>
          </form>
      </div>
@endsection
