@extends('layouts.plantillaPagErrors')
@section('title', '403')
@section('content-errores')
    <div class="fullpage-flex-center grey darken-4">
      <div class="flex-center pos-relative full-height">
        <div class="row">
          <div class="col s12">
            <div class="card center-align">
              <div class="card-content white-text pos-relative">
                <h2 class="warning-text">403</h2>
                <p class="caption grey-text">Opps! No tienes Acceso a esta Página!</p><a class="btn-floating halfway-fab waves-effect waves-light warning-bg"><i class="material-icons">home</i></a>
              </div>
              <div class="card-action grey darken-3"><a class="btn btn-flat white-text" href="{{ route('regresar') }}"><i class="material-icons left">Regresar a Página principal</i>Back</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
