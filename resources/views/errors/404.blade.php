@extends('layouts.plantillaPagErrors')
@section('title', '404')
@section('content-errores')
    <div class="fullpage-flex-center grey darken-4">
      <div class="flex-center pos-relative full-height">
        <div class="row">
          <div class="col s12">
            <div class="card center-align">
              <div class="card-content white-text pos-relative">
                <h2 class="primary-text">404</h2>
                <p class="caption grey-text">Lo sentimos, Esa Página no se encuntra</p><a class="btn-floating halfway-fab waves-effect waves-light primary-bg"><i class="material-icons">home</i></a>
              </div>
              <div class="card-action grey darken-3"><a class="btn btn-flat white-text" href="{{ route('regresar') }}"><i class="material-icons left">reply</i>Regresar a Página principal</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
