@extends('layouts.plantillaPagErrors')
@section('title', '500')
@section('content-errores')
    <div class="fullpage-flex-center grey darken-4">
      <div class="flex-center pos-relative full-height">
        <div class="row">
          <div class="col s12">
            <div class="card center-align">
              <div class="card-content white-text pos-relative">
                <h2 class="error-text">500</h2>
                <p class="caption grey-text text-accent-2">Whoop! Algo ocurrio con el Servidor Intente mas tarde!</p><a class="btn-floating halfway-fab waves-effect waves-light error-bg"><i class="material-icons">home</i></a>
              </div>
              <div class="card-action grey darken-3"><a class="btn btn-flat white-text" href="{{ route('regresar') }}"><i class="material-icons left">reply</i>Regresar Página Principal</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
