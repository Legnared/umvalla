@extends('layouts.plantilla-Coordinadores')
@section('titulo')
    Activos :: Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>PROFESORES</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
            <div class="col s12">
            <!-- Paginate	-->
            <div class="card-panel">
              <div class="row">
                <div class="col s12">
                  <div class="datatable-wrapper">
                    <table class="datatable-pagination mat-datatable display cell-border">
                      <thead>
                        <tr>
                          <th>Clave</th>
                          <th>Nombre</th>
                          <th>Apellido Paterno</th>
                          <th>Apellido Materno</th>
                          <th>Telefono</th>
                          <th>Correo</th>
                          <th>Especialidad</th>
                          <th>Estatus</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>PR001</td>
                          <td>Andrew</td>
                          <td>Salse</td>
                          <td>Rodriguez</td>
                          <td>4432176589</td>
                          <td>andrew@univalla.edu.mx</td>
                          <td>Matematicas</td>
                          <td>Activo</td>
                        </tr>
                        <tr>
                          <td>PR002</td>
                          <td>Karla Vivian</td>
                          <td>Castillo</td>
                          <td>Sepulveda</td>
                          <td>4431141819</td>
                          <td>karla@univalla.edu.mx</td>
                          <td>Matematicas</td>
                          <td>Activo</td>
                        </tr>
                        <tr>
                          <td>PR003</td>
                          <td>Salomon</td>
                          <td>Rodriguez</td>
                          <td>Rodriguez</td>
                          <td>4433578930</td>
                          <td>salomon@univalla.edu.mx</td>
                          <td>Sistemas</td>
                          <td>Activo</td>
                        </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!--</div>-->
    <!---->

@endsection
