@extends('layouts.plantilla-Coordinadores')
@section('titulo')
    Inicio :: Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Bienvenido</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
            <!-- Popout-->
      <div class="card-panel">
        <div class="row box-title">
          <div class="col s12">
            <h5>Plan de estudios</h5>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
              <li class="active">
                <div class="collapsible-header"><i class="material-icons">subtitles</i>1990 - 2000</div>
                <div class="collapsible-body">
                <table>
                  <tr>
                    <td>
                      <h5>Primer Semestre</h5>
                      <ul>
                        <li>Lectura y redacción</li>
                        <li>Fundamentos de animación</li>
                        <li>Geometría descriptiva</li>
                        <li>Historia del arte</li>
                        <li>Álgebra y trigonometría</li>
                        <li>Participación efectiva en equipos de trabajo</li>
                      </ul>
                    </td>
                    <td>
                      <h5>Quinto Semestre</h5>
                      <ul>
                        <li>Storyboarding</li>
                        <li>Caricatura y diseño de personajes</li>
                        <li>Animación 2D experta</li>
                        <li>Audio</li>
                        <li>Animación multimedia</li>
                        <li>Materiales, shaders y render procedural</li>
                        <li>Estructura de datos</li>
                        <li>Temas selectos de administración</li>
                        <li>Reconocimiento de los efectos de la globalización</li>
                      </ul>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <h5>Segundo Semestre</h5>
                      <ul>
                        <li>Guión básico</li>
                        <li>Dibujo básico</li>
                        <li>Animación 2D básica</li>
                        <li>Fundamentos de la animación 3D</li>
                        <li>Física</li>
                        <li>Temas selectos de dirección de negocios</li>
                        <li>Conocimiento de la realidad personal</li>
                      </ul>
                    </td>
                    <td>
                      <h5>Sexto Semestre</h5>
                      <ul>
                        <li>Escenarios</li>
                        <li>Ilustración digital en imágenes</li>
                        <li>Videojuegos básico</li>
                        <li>Efectos visuales y partículas avanzado</li>
                        <li>Materiales, shaders y render fotométrica</li>
                        <li>Metodología e investigación básica</li>
                        <li>Temas selectos de publicidad</li>
                        <li>Acercamiento a realidades alternas</li>
                      </ul>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <h5>Tercer Semestre</h5>
                      <ul>
                        <li>Guión avanzado</li>
                        <li>Dibujo anatómico</li>
                        <li>Animación 2D avanzada</li>
                        <li>Video</li>
                        <li>Multimedia básica</li>
                        <li>Modelado 3D</li>
                        <li>Programación</li>
                        <li>Temas selectos de costos y presupuestos</li>
                        <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>
                      </ul>
                    </td>
                    <td>
                      <h5>Septimo Semestre</h5>
                      <ul>
                        <li>Fotografía e imagen</li>
                        <li>Matte painting</li>
                        <li>Videojuegos avanzado</li>
                        <li>Actuación</li>
                        <li>Animación ( acting )</li>
                        <li>Metodología e investigación avanzada</li>
                        <li>Temas de actualidad en animación y videojuegos</li>
                        <li>Temas selectos de relaciones públicas</li>
                        <li>Diseño de proyectos de intervención social</li>
                      </ul>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Cuarto Semestre</h5>
                      <ul>
                        <li>Storyboarding</li>
                        <li>Caricatura y diseño de personajes</li>
                        <li>Animación 2D experta</li>
                        <li>Audio</li>
                        <li>Animación multimedia</li>
                        <li>Materiales, shaders y render procedural</li>
                        <li>Estructura de datos</li>
                        <li>Temas selectos de administración</li>
                        <li>Reconocimiento de los efectos de la globalización</li>
                      </ul>
                    </td>
                    <td>
                      <h5>Octavo Semestre</h5>
                      <ul>
                        <li>Stopmotion</li>
                        <li>Motion graphics</li>
                        <li>Cortometraje</li>
                        <li>Implementación de proyectos de intervención socia</li>
                      </ul>
                    </td>
                  </tr>
                </table>

              </div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2000 - 2005</div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología humana</li>
                          <li>Introducción a la Fisioterapia</li>
                          <li>Salud pública</li>
                          <li>Psicología y salud</li>
                          <li>Física aplicada al movimiento</li>
                          <li>Participación efectiva en equipos de trabajo</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Quinto Semestre</h5>
                        <ul>
                          <li>Laboratorio de kinesiología I</li>
                          <li>Teoría y práctica de rehabilitación comunitaria</li>
                          <li>Ortesis y Prótesis</li>
                          <li>Psicología en la rehabilitación</li>
                          <li>Interpretación de la realidad socioeconómica, política y cultura</li>
                          <li>Patología en la infancia II</li>
                          <li>Manejo de conflictos</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Segundo Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología del sistema músculo esquelético</li>
                          <li>Fisioterapia I</li>
                          <li>Desarrollo del niño</li>
                          <li>Bioquímica metabólica</li>
                          <li>Metodología de la investigación</li>
                          <li>Tecnologías de la información</li>
                          <li>Conocimiento de la realidad personal</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Sexto Semestre</h5>
                        <ul>
                          <li>Patología en la adolescencia y adultez</li>
                          <li>Laboratorio de kinesiología II</li>
                          <li>Rehabilitación en la discapacidad visual</li>
                          <li>Rehabilitación en el deportista</li>
                          <li>Terapia ocupacional</li>
                          <li>Bioética</li>
                          <li>Acercamiento a las realidades sociales alternas</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología del sistema nervioso</li>
                          <li>Fisioterapia II</li>
                          <li>Desarrollo del adolescente y del adulto</li>
                          <li>Biomecánica</li>
                          <li>Estadística</li>
                          <li>Pensamiento crítico</li>
                          <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Septimo Semestre</h5>
                        <ul>
                          <li>Patología en la tercera edad</li>
                          <li>Rehabilitación en la discapacidad auditiva</li>
                          <li>Rehabilitación respiratoria</li>
                          <li>Diseño de proyectos de intervención social</li>
                          <li>Prácticas clínicas I</li>
                          <li>Laboratorio de kinesiología III</li>
                          <li>Diseño de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h5>Cuarto Semestre</h5>
                        <ul>
                          <li>Patología en la infancia I</li>
                          <li>Propedéutica clínica aplicada a la rehabilitación</li>
                          <li>Desarrollo de la tercera edad</li>
                          <li>Reconocimiento a la mundialización y globalización en la cultura contemporánea</li>
                          <li>Farmacología aplicada a la rehabilitación</li>
                          <li>Comportamiento organizacional</li>
                          <li>Reconocimiento de los efectos de la globalización</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Octavo Semestre</h5>
                        <ul>
                          <li>Rehabilitación cardiovasclar</li>
                          <li>Prácticas clínicas II</li>
                          <li>Seminario de investigación</li>
                          <li>Rehabilitación sexual en la persona con discapacidad</li>
                          <li>Implementación de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2005 - 2010</div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Año</h5>
                        <ul>
                          <li>Antropología social</li>
                          <li>Psicología general I</li>
                          <li>El mundo del Nuevo Testamento</li>
                          <li>Antropología Filosófica</li>
                          <li>Evangelización y Catequesis</li>
                          <li>Metodología I</li>
                          <li>Revelación de Dios</li>
                          <li>Introducción a la Biblia</li>
                          <li>Seminario de investigación I</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Segundo Año</h5>
                        <ul>
                          <li>Diagnóstico de la realidad socioeducativa</li>
                          <li>Psicología general II</li>
                          <li>Historia de la Iglesia</li>
                          <li>Pedagogía I</li>
                          <li>Metodología II</li>
                          <li>Axiología y Catequesis</li>
                          <li>Teología del Nuevo Testamento I</li>
                          <li>Moral fundamental</li>
                          <li>Seminario de investigación II</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Año</h5>
                        <ul>
                          <li>Realidad cultural y social en América Latina</li>
                          <li>Realidad eclesial en América Latina</li>
                          <li>Pedagogía II</li>
                          <li>Metodología III</li>
                          <li>Antropología Teológica</li>
                          <li>Teología del Nuevo Testamento II</li>
                          <li>Cristología</li>
                          <li>Moral de la persona</li>
                          <li>Seminario de investigación III</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Cuarto Año</h5>
                        <ul>
                          <li>Credo y vida en Cristo en el catecismo de la Iglesia</li>
                          <li>Universal</li>
                          <li>Liturgia</li>
                          <li>Teología pastoral</li>
                          <li>Vida y misión del laico</li>
                          <li>Metodología Catequética I</li>
                          <li>Teología del Antiguo Testamento I</li>
                          <li>Metodología IV</li>
                          <li>Seminario de investigación IV</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Quinto Año</h5>
                        <ul>
                          <li>Espiritualidad bíblica</li>
                          <li>Mariología</li>
                          <li>Teología del Antiguo Testamento II</li>
                          <li>Doctrina social de la Iglesia</li>
                          <li>Teología sacramental</li>
                          <li>Medios de comunicación y pastoral</li>
                          <li>Innovación Catequética y generación de proyectos</li>
                          <li>Metodología Catequética II</li>
                        </ul>
                      </td>

                  </table>
                </div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2010 - 2015</div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Semestre</h5>
                        <ul>
                          <li>Introducción a la Economía</li>
                          <li>Fundamentos de Contabilidad</li>
                          <li>Teoría organizacional</li>
                          <li>Principios de Derecho para los negocios internacionales</li>
                          <li>Técnicas de redacción</li>
                          <li>Metodología de la investigación</li>
                          <li>Humanística I</li>
                          <li>Participación efectiva en equipos de trabajo</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Quinto Semestre</h5>
                        <ul>
                          <li>Economía de la empresa</li>
                          <li>Análisis de estados financieros</li>
                          <li>Estadística inferencial</li>
                          <li>Geopolítica y geoestratégica</li>
                          <li>Administración estratégica</li>
                          <li>Finanzas internacionales</li>
                          <li>Humanística V</li>
                          <li>Interpretación de la realidad socioeconómica, política y cultural</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Segundo Semestre</h5>
                        <ul>
                          <li>Microeconomía básica</li>
                          <li>Contabilidad administrativa</li>
                          <li>Cálculo diferencial</li>
                          <li>Teoría de las tendencias políticas</li>
                          <li>Estrategias de negocios</li>
                          <li>Derecho internacional público</li>
                          <li>Diseño y manejo de base de datos</li>
                          <li>Humanística II</li>
                          <li>Conocimiento de la realidad personal</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Sexto Semestre</h5>
                        <ul>
                            <li>Economía internacional</li>
                            <li>Formulación y evaluación de proyectos de inversión</li>
                            <li>Administración de operaciones</li>
                            <li>Mercado de activos financieros</li>
                            <li>Negocios internacionales</li>
                            <li>Estudios de caso de negocios internacionales</li>
                            <li>Humanística VI</li>
                            <li>Acercamiento a realidades alternas</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Semestre</h5>
                        <ul>
                          <li>Matemáticas financieras</li>
                          <li>Historia económica internacional</li>
                          <li>Taller de información para la toma de decisiones</li>
                          <li>Pronósticos de negocios</li>
                          <li>Derecho internacional privado</li>
                          <li>Aplicaciones informáticas</li>
                          <li>Humanística III</li>
                          <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Septimo Semestre</h5>
                        <ul>
                          <li>Seminario de relaciones económicas internacionales</li>
                          <li>Estándares internacionales de Contabilidad</li>
                          <li>Planeación financiera</li>
                          <li>Mercado de derivados</li>
                          <li>Operaciones de comercio internacional</li>
                          <li>Taller de simulación de negocios</li>
                          <li>Humanística VII</li>
                          <li>Diseño de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h5>Cuarto Semestre</h5>
                        <ul>
                          <li>Macroeconomía básica</li>
                          <li>Estadística descriptiva</li>
                          <li>Historia económica y diplomática </li>
                          <li>México-EUA</li>
                          <li>Empresas mundiales</li>
                          <li>Mercadotecnia internacional</li>
                          <li>Derecho económico y organismos internacionales</li>
                          <li>Humanística IV</li>
                          <li>Reconocimiento de los efectos de la globalización</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Octavo Semestre</h5>
                        <ul>
                          <li>Seminario de dirección y administración internacional</li>
                          <li>Seminario de relaciones financieras internacionales</li>
                          <li>Organización y perspectivas de los negocios internacionales</li>
                          <li>Seminario de relaciones jurídicas internacionales</li>
                          <li>Seminario de investigación de casos en tratados económicos</li>
                          <li>Humanística VIII</li>
                          <li>Implementación de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
              </li>

              <li>
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2015 - 2020</div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Semestre</h5>
                        <ul>
                          <li>Cálculo diferencial</li>
                          <li>Introducción a la computación</li>
                          <li>Introducción a la Ingeniería</li>
                          <li>Técnicas de investigación documental</li>
                          <li>Dibujo y Geometría descriptiva</li>
                          <li>Química</li>
                          <li>Participación efectiva en equipos de trabajo</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Quinto Semestre</h5>
                        <ul>
                          <li>Ingeniería de sistemas</li>
                          <li>Administración de la calidad</li>
                          <li>Instrumentación industrial</li>
                          <li>Procesos de manufactura</li>
                          <li>Legislación industrial</li>
                          <li>Métodos numéricos</li>
                          <li>Contabilidad y finanzas</li>
                          <li>Interpretación de la realidad socioeconómica, política y cultural</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Segundo Semestre</h5>
                        <ul>
                          <li>Álgebra lineal</li>
                          <li>Cálculo integral</li>
                          <li>Estática</li>
                          <li>Dibujo asistido por computadora</li>
                          <li>Administración para Ingenieros</li>
                          <li>Herramientas computacionales</li>
                          <li>Conocimiento de la realidad personal</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Sexto Semestre</h5>
                        <ul>
                          <li>Ingeniería eléctrica</li>
                          <li>Modelos de simulación</li>
                          <li>Teoría de sistemas</li>
                          <li>Aseguramiento y auditoría de la calidad</li>
                          <li>Manufactura integrada por computadora</li>
                          <li>Probabilidad y estadística aplicada</li>
                          <li>Acercamiento a realidades sociales alternas</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Semestre</h5>
                        <ul>
                          <li>Dinámica</li>
                          <li>Cálculo multivariable</li>
                          <li>Electricidad y magnetismo</li>
                          <li>Circuitos eléctricos</li>
                          <li>Programación lineal</li>
                          <li>Estudio del trabajo</li>
                          <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>

                        </ul>
                      </td>
                      <td>
                        <h5>Septimo Semestre</h5>
                        <ul>
                          <li>Distribución y logística</li>
                          <li>Administración de plantas industriales</li>
                          <li>Administración de la producción I</li>
                          <li>Diseño de experimentos</li>
                          <li>Ingeniería económica y financiera</li>
                          <li>Higiene y seguridad industrial</li>
                          <li>Diseño de proyectos de intervención social</li>

                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h5>Cuarto Semestre</h5>
                        <ul>
                          <li>Termodinámica</li>
                          <li>Tecnología de materiales</li>
                          <li>Ecuaciones diferenciales</li>
                          <li>Electrónica básica</li>
                          <li>Planificación y control de proyectos</li>
                          <li>Reconocimiento de los efectos de la mundialización y globalización en la cultura contemporánea</li>

                        </ul>
                      </td>
                      <td>
                        <h5>Octavo Semestre</h5>
                        <ul>
                          <li>Relaciones industriales</li>
                          <li>Administración de la producción II</li>
                          <li>Desarrollo de habilidades gerenciales</li>
                          <li>Proyecto terminal</li>
                          <li>Evaluación de proyectos</li>
                          <li>Sistemas de organización</li>
                          <li>Implementación de proyectos de Intervención social</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
              </li>
              <li>
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2020 - 2025</div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Semestre</h5>
                        <ul>
                          <li>Cultura jurídica</li>
                          <li>Teoría del Estado</li>
                          <li>Obligaciones</li>
                          <li>Manejo de sistemas informáticos en el Derecho</li>
                          <li>Derecho constitucional</li>
                          <li>Clínica de Derecho: Expresión legal oral y escrita</li>
                          <li>Participación efectiva en equipos de trabajo</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Quinto Semestre</h5>
                        <ul>
                          <li>Amparo</li>
                          <li>Derecho Administrativo</li>
                          <li>Ciencias forenses</li>
                          <li>Propiedad industrial e intelectual</li>
                          <li>Clínica de procedimientos especiales</li>
                          <li>Interpretación de la realidad </li>
                          <li>Interpretación de la realidad socioeconómica, política y cultural</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Segundo Semestre</h5>
                        <ul>
                          <li>Finanzas</li>
                          <li>Derecho civil: generalidades, personas y familia</li>
                          <li>Derecho penal: Parte general</li>
                          <li>Títulos y operaciones de crédito</li>
                          <li>Clínica inicial de derecho privado y empresarial</li>
                          <li>Conocimiento de la realidad personal</li>

                        </ul>
                      </td>
                      <td>
                        <h5>Sexto Semestre</h5>
                        <ul>
                          <li>Técnicas de Argumentación Jurídica</li>
                          <li>Competencia Económica y Protección al Consumidor</li>
                          <li>Derecho Internacional Público y Privado</li>
                          <li>Derecho de la Seguridad Social</li>
                          <li>Clínica de Amparo</li>
                          <li>Acercamiento a Realidades Alternas</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Semestre</h5>
                        <ul>
                          <li>Derecho civil de los bienes</li>
                          <li>Derecho penal: Parte especial</li>
                          <li>Sociedades mercantiles</li>
                          <li>Teoría general del proceso</li>
                          <li>Clínica de derecho procesal</li>
                          <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Septimo Semestre</h5>
                        <ul>
                          <li>Derechos Humanos</li>
                          <li>Política criminal</li>
                          <li>Prácticas profesionales</li>
                          <li>Derecho urbanístico</li>
                          <li>Clínica de globalización e interdependencia nacional e internacional</li>
                          <li>Diseño de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h5>Cuarto Semestre</h5>
                        <ul>
                          <li>Derecho Aduanero</li>
                          <li>Derecho Fiscal</li>
                          <li>Derecho Laboral</li>
                          <li>Derecho Ambiental</li>
                          <li>Clínica de Derecho en la regulación</li>
                          <li>Laboral y del comercio</li>
                          <li>Reconocimiento de los efectos de la mundialización y la globalización en la cultura moderna</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Octavo Semestre</h5>
                        <ul>
                          <li>Derecho Notarial y correduría pública</li>
                          <li>Planeación financiera y procedimiento de auditoría legal</li>
                          <li>Arbitraje y medios de solución de conflictos</li>
                          <li>Diagnóstico de soluciones legales</li>
                          <li>Metodología de la investigación jurídica</li>
                          <li>Implementación de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
              </li>
              
              
@endsection
