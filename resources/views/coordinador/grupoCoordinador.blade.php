@extends('layouts.plantilla-Coordinadores')
@section('titulo')
    Grupo :: Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>GRUPOS</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row center">
                        <h5>ASIGNAR GRUPOS A PROFESORES</h5>
                          <div class="datatable-wrapper">
                                <table class="datatable-badges display cell-border">
                                  <thead>
                                    <tr>
                                      <th>Clave de Grupo</th>
                                      <th>Materia</th>
                                      <th>Grupo</th>
                                      <th>No.Alumnos</th>
                                      <th>Profesor</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>EJ01</td>
                                      <td>Anatomía y Fisiología Humana</td>
                                      <td>A</td>
                                      <td>25</td>
                                      <td>Julian Albero Perez Valenzuela</td>
                                      <td>
                                        <div class="action-btns">
                                            <a class="btn-floating warning-bg" href="{{ route('asignarCoordinador') }}" title="Editar"><i class="material-icons">edit</i>
                                            </a>
                                            <a class="btn-floating info-bg" href="javascript:void(0)"><i class="material-icons">import_export</i></a>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>EJ01</td>
                                      <td>Anatomía y Fisiología Humana II</td>
                                      <td>A</td>
                                      <td>25</td>
                                      <td></td>
                                      <td>
                                        <div class="action-btns">
                                            <a class="btn-floating warning-bg" href="{{ route('asignarCoordinador') }}" title="Editar"><i class="material-icons">edit</i>
                                            </a>
                                            <a class="btn-floating info-bg" href="javascript:void(0)"><i class="material-icons">import_export</i></a>
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                                  </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection

