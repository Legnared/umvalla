@extends('layouts.plantilla-Coordinadores')
@section('titulo')
    Kardex Alumnos :: Coordinadores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Kardex Alumnos</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">

  <div class="row">
    <div class="input-field col s3">
              <input type="text" name="contr0l" />
                <label>Numero de control</label>
            </div>
            <br>
           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
           <a class="waves-effect waves-light btn">Buscar</a>
         </div> <br><br>
          <div class="datatable-wrapper">
              <H6>CERVANTES VAZQUEZ ANA ISABEL</H6><br>
              <table class="datatable-badges display cell-border">
                <center>
                <h5>Periodo Agosto - Diciembre 2018</h5> </center>
                <thead>
                  <tr>
                    <th>Materia</th>
                    <th>Grupo</th>
                    <th>Calificacion</th>
                    <th>Asistencias</th>
                    <th>Evaluacion</th>
                    <th>Observaciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Ingles</td>
                    <td>A</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Español</td>
                    <td>B</td>
                    <td>95</td>
                    <td>9</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Calculo</td>
                    <td>A</td>
                    <td>100</td>
                    <td>10</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Sustentabilidad</td>
                    <td>A</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <th>Promedio Sem.</th>
                    <td></td>
                    <th>89</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              <br><br>
              <table class="datatable-badges display cell-border">
                <center>
                <h5>Periodo Enero - Junio 2019</h5> </center>
                <br>
                <thead>
                  <tr>
                    <th>Materia</th>
                    <th>Grupo</th>
                    <th>Calificacion</th>
                    <th>Asistencias</th>
                    <th>Evaluacion</th>
                    <th>Observaciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Ingles</td>
                    <td>A</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Español</td>
                    <td>B</td>
                    <td>95</td>
                    <td>9</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Calculo</td>
                    <td>A</td>
                    <td>100</td>
                    <td>10</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Sustentabilidad</td>
                    <td>A</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <th>Promedio Sem.</th>
                    <td></td>
                    <th>89</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
      </div>
    </div>
</div>
@endsection
