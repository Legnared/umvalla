@extends('layouts.plantilla-Profesores')
@section('titulo')
    Inicio :: Profesores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Bienvenido</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row center">
                        <div id="userProfileSettings">
                        <div class="row">
                          <form class="col s12 profile-info-form" method="" action="#">
                            <div class="card-panel profile-form-cardpanel">
                              <div class="row box-title">
                                <div class="col s12">
                                  <h5>Información de Perfil</h5>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">person</i>
                                  <input class="validate" type="text" id="first-name" name="nombre" value="Roberto">
                                  <label for="first-name">Nombre</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                                  <input class="validate" type="text" id="last_name" name="apellido" value="Gutierrez sanchéz">
                                  <label for="last_name">Apellidos</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">edit_location</i>
                                  <input class="validate" type="text" id="addressline1" name="direccion" value="Paseo del abedul #150">
                                  <label for="addressline1">Dirección</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">edit_location</i>
                                  <input class="validate" type="text" id="addressline2" name="colonia" value="Jacarandas">
                                  <label for="addressline2">Colonia</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">location_city</i>
                                  <input class="validate" type="text" id="city" name="ciudad" value="Morelia">
                                  <label for="city">Ciudad</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">my_location</i>
                                  <input class="validate" type="text" id="state" name="municipio" value="Michoacán">
                                  <label for="state">Municipio</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">confirmation_number</i>
                                  <input class="validate" type="text" id="zipcode" name="cp" value="58120">
                                  <label for="zipcode">C.P.</label>
                                </div>
                                <div class="input-field col s12 m6">
                                  <select class="basic-select" name="pais" id="country">
                                    <option value="" disabled selected>Selecciona pais</option>
                                    <option value="us" selected>México</option>
                                    <option value="in">India</option>
                                    <option value="...">...</option>
                                    <option value="ca">Canada</option>
                                    <option value="ch">China</option>
                                  </select>
                                  <label for="country">Country</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">email</i>
                                  <input class="validate" type="email" id="email" name="email" value="thoriseum@gmail.com">
                                  <label for="email">Email</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">Telefono</i>
                                  <input class="validate" type="text" id="mobile" name="mobile" value="4440888710">
                                  <label for="mobile">Celular</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12 right-align">
                                  <button class="btn waves-effect waves-set" type="submit" name="update_profile">Update<i class="material-icons right">save</i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection
