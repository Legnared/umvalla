@extends('layouts.plantilla-Profesores')
@section('titulo')
    Historico Grupos :: Profesores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>GRUPOS</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">
            <div class="row">
                <div class="row" style="margin-top:10px !important;">
                    <div class="col s12">
                          <div class="card-panel">
                            <div class="row box-title">
                              <div class="col s12">
                                <h5 class="content-headline">LISTA DE CALIFICACIONES</h5>
                                <p>Materia</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s12">
                                <div class="datatable-wrapper">
                                  <table class="datatable-badges display cell-border">
                                    <thead>
                                      <tr>
                                        <th>Nombre del Alumno</th>
                                        <th>No. Control</th>
                                        <th>Periodo 1</th>
                                        <th>Periodo 2</th>
                                        <th>Periodo 3</th>
                                        <th>Calificacion Final</th>
                                        <th>Faltas</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <th>Jorge Torres Mendoza</th>
                                        <th>131201</th>
                                        <th>80</th>
                                        <th>70</th>
                                        <th>70</th>
                                        <th>75</th>
                                        <th>2</th>
                                      </tr>
                                      <tr>
                                        <th>Karla Rodriguez Valle</th>
                                        <th>131202</th>
                                        <th>85</th>
                                        <th>90</th>
                                        <th>90</th>
                                        <th>88</th>
                                        <th>2</th>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
