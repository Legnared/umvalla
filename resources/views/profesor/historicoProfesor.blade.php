@extends('layouts.plantilla-Profesores')
@section('titulo')
    Inicio :: Profesores
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>GRUPOS</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row center">
                        <h5>Historico de Grupos</h5>
                         <br><br>
                              <div class="datatable-wrapper">
                                <table class="datatable-badges display cell-border">
                                  <thead>
                                    <tr>
                                      <th>Periodo</th>
                                      <th>Materia</th>
                                      <th>Grupo</th>
                                      <th>Carrera</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Enero - Junio 2019</td>
                                      <td>Administracion de Procesos</td>
                                      <td>AP01</td>
                                      <td>Licenciatura en Sistemas</td>
                                      <td>
                                        <div class="action-btns">
                                            <a class="btn-floating info-bg" href="{{ route('listasProfesor') }}"><i class="material-icons">import_export</i></a>
                                        </div>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection
