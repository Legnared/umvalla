@extends('layouts.plantilla-Alumnos')
@section('titulo')
    Kardex :: Alumno
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>KARDEX</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">  
          <div class="datatable-wrapper">
              <table class="datatable-badges display cell-border">
                <center>
                <h5>Lista De Calificaciones</h5></center>
                <thead>
                  <tr>
                    <th>Semestre</th>
                    <th>Clave</th>
                    <th>Materia</th>
                    <th>Parcial 1</th>
                    <th>Parcial 2</th>
                    <th>Parcial 3</th>
                    <th>Inasistencias</th>
                    <th>Eval. Final</th>
                    <th>Evaluacion</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>1</th>
                    <td>IN0301</td>
                    <td>Ingles</td>
                    <td>90</td>
                    <td>89</td>
                    <td>91</td>
                    <td>15</td>
                    <td>90</td>
                    <td>Ordinaria</td>                    
                    </td>
                  </tr>
                  <tr>
                    <th>1</th>
                    <td>IN0302</td>
                    <td>Español</td>
                    <td>86</td>
                    <td>95</td>
                    <td>77</td>
                    <td>9</td>
                    <td>85</td>
                    <td>Ordinaria</td>
                  </td>
                  </tr>
                  <tr>
                    <th>1</th>
                    <td>IN0303</td>
                    <td>Calculo</td>
                    <td>90</td>
                    <td>100</td>
                    <td>80</td>
                    <td>10</td>
                    <td>90</td>
                    <td>Ordinaria</td>         
                  </tr>
                  <tr>
                    <th>1</th>
                    <td>IN0304</td>
                    <td>Sustentabilidad</td>
                    <td>77</td>
                    <td>89</td>
                    <td>94</td>
                    <td>15</td>
                    <td>88</td>
                    <td>Ordinaria</td>                    
                  </tr>
                  <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <th>Promedio</th>
                  <th>9.0</th>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
      </div>
    </div>
</div>
@endsection
