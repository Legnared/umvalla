@extends('layouts.plantilla-Alumnos')
@section('titulo')
    Contacto :: Alumno
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Bienvenido</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                    <div class="row center">
                        <div id="userProfileSettings">
                        <div class="row">
                          <form class="col s12 profile-info-form" method="" action="">
                            <div class="card-panel profile-form-cardpanel">
                              <div class="row box-title">
                                <div class="col s12">
                                  <h5>Datos del Contacto</h5>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">person</i>
                                  <input class="validate" type="text" id="first-name" name="nombre" value="Roberto">
                                  <label for="first-name">Nombre</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">person_outline</i>
                                  <input class="validate" type="text" id="last_name" name="apellido" value="Gutierrez sanchéz">
                                  <label for="last_name">Apellidos</label>
                                </div>
                              </div>
                                                         
                              <div class="row">
                                <div class="input-field col m6 s12"><i class="material-icons prefix">Telefono</i>
                                  <input class="validate" type="text" id="mobile" name="mobile" value="4440888710">
                                  <label for="mobile">Celular</label>
                                </div>
                                <div class="input-field col m6 s12"><i class="material-icons prefix">email</i>
                                  <input class="validate" type="email" id="email" name="email" value="thoriseum@gmail.com">
                                  <label for="email">Email</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12 right-align">
                                  <button class="btn waves-effect waves-set" type="submit" name="">Update<i class="material-icons right">save</i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection