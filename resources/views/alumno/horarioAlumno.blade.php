@extends('layouts.plantilla-Alumnos')
@section('titulo')
    Calificacion :: Alumno
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Horario</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
    <div class="row">
          <div class="col s12">
          </div>
          <!-- Collections-->
          <div class="col s12">
            <div class="card-panel">
              <div class="row box-title">
                <div class="col s12">
                  <h5>Materias</h5>
                    <div class="row center">
                      <table>
                        <tr>
                        <td>
                      <div class="col s13">
                        <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                          <li class="active">
                            <div class="collapsible-header"><i class="material-icons">subtitles</i>AB1-Fisioterapia II</div>
                            <div class="collapsible-body">
                              <p>Lunes a viernes 9:00 AM</p>
                            </div>
                          </div>
                        </td>
                        <td>
                          <div class="col s13">
                            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                              <li class="active">
                                <div class="collapsible-header"><i class="material-icons">subtitles</i>AB2-Álgebra y trigonometría</div>
                                <div class="collapsible-body">
                                  <p>Lunes, Miercoles y Viernes 11:00 AM</p>
                                </div>
                              </div>
                            </td>
                            <td>
                              <div class="col s13">
                                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                  <li class="active">
                                    <div class="collapsible-header"><i class="material-icons">subtitles</i>BC1-Lectura y redacción</div>
                                    <div class="collapsible-body">
                                      <p>Lunes, Martes y Viernes 13:00 PM</p>
                                    </div>
                                  </div>
                                </td>
                                </tr>

                                <tr>
                                <td>
                              <div class="col s13">
                                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                  <li class="active">
                                    <div class="collapsible-header"><i class="material-icons">subtitles</i>CA1-Metodología I</div>
                                    <div class="collapsible-body">
                                      <p>Martes y Viernes 16:00 PM</p>
                                    </div>
                                  </div>
                                </td>
                                <td>
                                  <div class="col s13">
                                    <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                      <li class="active">
                                        <div class="collapsible-header"><i class="material-icons">subtitles</i>CB2-Estadística descriptiva</div>
                                        <div class="collapsible-body">
                                          <p>Lunes, Martes y Jueves 12:00 PM</p>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="col s13">
                                        <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                          <li class="active">
                                            <div class="collapsible-header"><i class="material-icons">subtitles</i>AC1-Planeación financiera</div>
                                            <div class="collapsible-body">
                                              <p>Lunes y Viernes 8:00 AM</p>
                                            </div>
                                          </div>
                                        </td>
                                        </tr>
                                </table>
                        </div>


                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
@endsection

