@extends('layouts.plantilla-Alumnos')
@section('titulo')
    Plan de Estudios :: Alumno
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Plan de Estudios</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
            <!-- Popout-->
      <div class="card-panel">
        <div class="row box-title">
          <div class="col s12">
            <h5>Licenciatura en Fisioterapia y Rehabilitación</h5>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
              <li class="active">
                <div class="collapsible-header"><i class="material-icons">subtitles</i>2010 - 2020 </div>
                <div class="collapsible-body">
                  <table>
                    <tr>
                      <td>
                        <h5>Primer Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología humana</li>
                          <li>Introducción a la Fisioterapia</li>
                          <li>Salud pública</li>
                          <li>Psicología y salud</li>
                          <li>Física aplicada al movimiento</li>
                          <li>Participación efectiva en equipos de trabajo</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Quinto Semestre</h5>
                        <ul>
                          <li>Laboratorio de kinesiología I</li>
                          <li>Teoría y práctica de rehabilitación comunitaria</li>
                          <li>Ortesis y Prótesis</li>
                          <li>Psicología en la rehabilitación</li>
                          <li>Interpretación de la realidad socioeconómica, política y cultura</li>
                          <li>Patología en la infancia II</li>
                          <li>Manejo de conflictos</li>

                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Segundo Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología del sistema músculo esquelético</li>
                          <li>Fisioterapia I</li>
                          <li>Desarrollo del niño</li>
                          <li>Bioquímica metabólica</li>
                          <li>Metodología de la investigación</li>
                          <li>Tecnologías de la información</li>
                          <li>Conocimiento de la realidad personal</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Sexto Semestre</h5>
                        <ul>
                          <li>Patología en la adolescencia y adultez</li>
                          <li>Laboratorio de kinesiología II</li>
                          <li>Rehabilitación en la discapacidad visual</li>
                          <li>Rehabilitación en el deportista</li>
                          <li>Terapia ocupacional</li>
                          <li>Bioética</li>
                          <li>Acercamiento a las realidades sociales alternas</li>
                        </ul>
                      </td>
                    </tr>

                    <tr>
                      <td>
                        <h5>Tercer Semestre</h5>
                        <ul>
                          <li>Anatomía y Fisiología del sistema nervioso</li>
                          <li>Fisioterapia II</li>
                          <li>Desarrollo del adolescente y del adulto</li>
                          <li>Biomecánica</li>
                          <li>Estadística</li>
                          <li>Pensamiento crítico</li>
                          <li>Toma de conciencia de la naturaleza humana en el contexto histórico</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Septimo Semestre</h5>
                        <ul>
                          <li>Patología en la tercera edad</li>
                          <li>Rehabilitación en la discapacidad auditiva</li>
                          <li>Rehabilitación respiratoria</li>
                          <li>Diseño de proyectos de intervención social</li>
                          <li>Prácticas clínicas I</li>
                          <li>Laboratorio de kinesiología III</li>
                          <li>Diseño de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <h5>Cuarto Semestre</h5>
                        <ul>
                          <li>Patología en la infancia I</li>
                          <li>Propedéutica clínica aplicada a la rehabilitación</li>
                          <li>Desarrollo de la tercera edad</li>
                          <li>Reconocimiento a la mundialización y globalización en la cultura contemporánea</li>
                          <li>Farmacología aplicada a la rehabilitación</li>
                          <li>Comportamiento organizacional</li>
                          <li>Reconocimiento de los efectos de la globalización</li>
                        </ul>
                      </td>
                      <td>
                        <h5>Octavo Semestre</h5>
                        <ul>
                          <li>Rehabilitación cardiovasclar</li>
                          <li>Prácticas clínicas II</li>
                          <li>Seminario de investigación</li>
                          <li>Rehabilitación sexual en la persona con discapacidad</li>
                          <li>Implementación de proyectos de intervención social</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                </div>
              </li>
              
@endsection
