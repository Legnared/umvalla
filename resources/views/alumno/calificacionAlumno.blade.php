@extends('layouts.plantilla-Alumnos')
@section('titulo')
    Calificacion :: Alumno
@endsection
@section('titulos-cabezera')
    <div class="sec-page">
      <div class="page-title">
        <h2>Calificaciones Periodo Actual</h2>
      </div>
      <div class="page-options">
      </div>
    </div>
@endsection
@section('contenido')
<div class="row">
    <div class="card-panel">
      <div class="row box-title">
        <div class="col s12">

  <div class="row">
    <div class="input-field col s12 m3">
            
           </div>
            <div class="input-field col s3">
            </div>
                   
         </div> 
          <div class="datatable-wrapper">
              <table class="datatable-badges display cell-border">
                <center>
                <h5>Periodo Agosto - Diciembre 2018</h5></center>
                <thead>
                  <tr>
                    <th>Materia</th>
                    <th>Grupo</th>
                    <th>Parcial 1</th>
                    <th>Parcial 2</th>
                    <th>Parcial 3</th>
                    <th>Calif. Final</th>
                    <th>Inasistencias</th>
                    <th>Evaluacion</th>
                    <th>Observaciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Ingles</td>
                    <td>A</td>
                    <td>89</td>
                    <td>89</td>
                    <td>89</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Español</td>
                    <td>B</td>
                    <td>95</td>
                    <td>95</td>
                    <td>95</td>
                    <td>95</td>
                    <td>9</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Calculo</td>
                    <td>A</td>
                    <td>100</td>
                    <td>100</td>
                    <td>100</td>
                    <td>100</td>
                    <td>10</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td>Sustentabilidad</td>
                    <td>A</td>
                    <td>89</td>
                    <td>89</td>
                    <td>89</td>
                    <td>89</td>
                    <td>15</td>
                    <td>Ordinaria</td>
                    <td>Ninguna</td>
                    <td>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
      </div>
    </div>
</div>
@endsection