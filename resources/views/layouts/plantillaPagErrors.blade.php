<!DOCTYPE html>
<html class="loading" lang="es">
  <head>
    <link rel="icon" href="../images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../images/favicon/favicon-32x32.png">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sistema creado para la universidad Marista.">
    <meta name="keywords" content="Colaboradores, Angel Guillermo Hernandez Zambrano, Ricardo Ascencio Flores, Berenice Villa Fuerte, Gustavo, Julieta Denisse, Oscar Vallejo Tapia">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <link rel="icon" href="../styles/images/favicon/favi.ico" sizes="32x32">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>@yield('titulo')</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/isolata-font.css" type="text/css">
    <link rel="stylesheet" href="../styles/css/icon.css" type="text/css">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/main.css">
    <link rel="stylesheet" href="../styles/css/dynamic.min.css">
    <link rel="stylesheet" href="../styles/css/markup.min.css">
    <link rel="stylesheet" href="../styles/plugins/scrollbar/perfect-scrollbar.min.css">
  </head>
  <body class="error-page">
    <!-- ============================-->
    <!-- PRELOAD-->
    <!-- ============================-->
    <div id="preloader">
      <div class="preloader-center">
        <div class="dots-loader dot-circle"></div>
      </div>
    </div>
    <!-- ============================-->
    <!-- CONTENT AREA-->
    @yield('content-errores')
    
    <!-- ============================-->
    <!-- ============================-->
    <!-- SCRIPTS-->
    <!-- ============================-->
    <script type="text/javascript" src="../styles/js/all.js"></script>
     </body>
</html>
