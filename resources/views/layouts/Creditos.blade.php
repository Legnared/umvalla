<!DOCTYPE html>
<html class="loading" lang="es">
  <head>
    <link rel="icon" href="../images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../images/favicon/favicon-32x32.png">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sistema creado para la universidad Marista.">
    <meta name="keywords" content="Colaboradores, Angel Guillermo Hernandez Zambrano, Ricardo Ascencio Flores, Berenice Villa Fuerte, Gustavo, Julieta Denisse, Oscar Vallejo Tapia">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <link rel="icon" href="../styles/images/favicon/favi.ico" sizes="32x32">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>@yield('titulo')</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/isolata-font.css" type="text/css">
    <link rel="stylesheet" href="../styles/css/icon.css" type="text/css">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/main.css">
    <link rel="stylesheet" href="../styles/css/dynamic.min.css">
    <link rel="stylesheet" href="../styles/css/markup.min.css">
    <link rel="stylesheet" href="../styles/plugins/scrollbar/perfect-scrollbar.min.css">
  </head>
  <body class="error-page">
    <!-- ============================-->
    <!-- PRELOAD-->
    <!-- ============================-->
    <div id="preloader">
      <div class="preloader-center">
        <div class="dots-loader dot-circle"></div>
      </div>
    </div>
    <!-- ============================-->
    <!-- CONTENT AREA-->
    <div class="main-container">
        <div class="row">
            <div class="col s12 m6 l4 user-widget">
              <div class="card blue-grey darken-1 user-widget-type4">
                <div class="card-image"><img class="responsive-img" src="../styles/images/angel-guillermo.jpg" alt="placeholder">
                  <div class="card-title center">Ángel Guillermo Hernandez Zambrano</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
                </div>
                <div class="card-content">
                  <div class="designation center grey-text text-darken-3">Lider, Frontend, Product Owner, Analista,Testing and Documenting</div>
                  <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/Uryoteck"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/Uryoteck"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/Uryoteck"><i class="mdi mdi-github-circle"></i></a>
                      <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/Uryoteck"><i class="mdi mdi-google-plus"></i></a></div>
                </div>
                <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ángel Guillermo Hernandez Zambrano<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia Actualmente curso 9 Semestre de la Carrera de Ing.Informática.</p>
                  <p>Mi Rol aparte de Lider de Proyecto es Realizar Interfaz Front del Sistema en su Totalidad.</p>
                  <p>Correo de Gmail: m3m.angel1761@gmail.com</p>
                  <p>Facebook: Guillermo Hernandez.</p>
                  <p>GitLab: Angel Guillermo@Legnared</p>
                </div>
              </div>
            </div>
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img class="responsive-img" src="../styles/images/Berenice.jpg" alt="placeholder">
                <div class="card-title center">Berenice Villafuerte Zavala</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Back-End, Testing and Documenting</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Berenice Villafuerte Zavalar<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia. Actualmente curso 7 Semestre de la Carrera de ITIC's.</p>
                  <p>Mi rol es Analista  y desarrollador de Backend.</p>
                  <p>Correo de Gmail: zavala0555@gmail.com</p>
                  <p>Facebook: Berenice V Zavala.</p>
                  <p>GitLab: Berenice005.</p>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img src="../styles/images/ricardo.jpg" alt="placeholder" ,class="responsive-img">
                <div class="card-title center">Ricardo Ascencio Flores</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Backend</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Ricardo Ascencio Flores<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia. Actualmente curso 9 Semestre de la Carrera de Ingenieria en Sistemas Computacionales.</p>
                  <p>Mi rol es Analista  y desarrollador de Backend.</p>
                  <p>Correo de Gmail:  titis.11235@gmail.com.</p>
                  <p>Facebook: Ricardo Ascencio.</p>
                  <p>GitLab: Rick742.</p>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img src="../styles/images/denisse.jpg" alt="placeholder" ,class="responsive-img">
                <div class="card-title center">Julieta Denisse Huerta Cortes</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Designer</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Julieta Denisse Huerta Cortes<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia. Actualmente curso 7 Semestre de la Carrera de ITIC's.</p>
                  <p>Mi rol es Diseñador  y desarrollador de Frontend.</p>
                  <p>Correo de Gmail:  denissehuertac@gmail.com.</p>
                  <p>Facebook: Julieta Denisse Huerta.</p>
                  <p>GitLab: DenisseHuerta.</p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img class="responsive-img" src="../styles/images/gustavo.jpg" alt="placeholder">
                <div class="card-title center">Gustavo Adolfo Diana Albarrán</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Frontend</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/gustavoada"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/gustavoada"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="https://www.facebook.com/gustavoada"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Gustavo Adolfo Diana Albarrán<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia. Actualmente curso 7 Semestre de la Carrera de ITIC's.</p>
                  <p>Mi rol es Diseñador  y desarrollador de Frontend.</p>
                  <p>Correo de Gmail:  gus.adolfo.10@gmail.com.</p>
                  <p>Facebook: https://www.facebook.com/gustavoada.</p>
                  <p>GitLab: GustDian.</p>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img src="../styles/images/oscar.jpg" alt="placeholder" ,class="responsive-img">
                <div class="card-title center">Oscar Vallejo Tapia</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Analista Data Base y Back-end</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Oscar Vallejo Tapia<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia. Actualmente curso 9 Semestre de la Carrera de Ingeniería en Sistemas Computacionales.</p>
                  <p>Mi rol es Analista de Base de Datos y Desarrollador de Backend.</p>
                  <p>Correo de Gmail: oscar.vallejo.tapia95@gmail.com</p>
                  <p>Facebook: Oscaa'r Vallejo.</p>
                  <p>Git: oscar_vallejo_tapia</p>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l4 user-widget">
            <div class="card blue-grey darken-1 user-widget-type4">
              <div class="card-image"><img src="../styles/images/daniel.jpg" alt="placeholder" ,class="responsive-img">
                <div class="card-title center">Daniel Espinoza Martínez</div><a class="btn-floating np-bg btn-flat waves-effect waves-light activator" href="javascript:void(0)"><i class="material-icons grey-text">more_vert</i></a>
              </div>
              <div class="card-content">
                <div class="designation center grey-text text-darken-3">Back-End</div>
                <div class="socialization center"><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-facebook"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-twitter"></i></a><a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-github-circle"></i></a>
                    <a class="btn-floating  grey darken-3 btn-flat waves-effect waves-light" href="#!"><i class="mdi mdi-google-plus"></i></a></div>
              </div>
              <div class="card-reveal"><span class="card-title grey-text text-darken-4">Daniel Espinoza Martínez<i class="material-icons right">close</i></span>
                  <p>Soy estudiante de Tecnologico de Morelia Actualmente curso 8 Semestre de la Carrera de Ingeniería en Sistemas Computacionales.</p>
                  <p>Soy Desallorador Interfaz Back-end del Sistema.</p>
                  <p>Correo de Gmail: danielespinoza.dem@gmail.com</p>
                  <p>Facebook: Daniel Espinoza Martinez.</p>
                  <p>Git: Dany11Esp</p>
              </div>
            </div>
          </div>
        </div>
    </div>


    <!-- ============================-->
    <!-- ============================-->
    <!-- SCRIPTS-->
    <!-- ============================-->
    <script type="text/javascript" src="../styles/js/all.js"></script>
     </body>
</html>
