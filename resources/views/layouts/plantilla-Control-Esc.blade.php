<!DOCTYPE html>
<html class="loading" lang="es">
  <head>
    <script src="/js/metodosumvalla/recursosumvalla.js" type="text/javascript">

    </script>

    <link rel="icon" href="../images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../images/favicon/favicon-32x32.png">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sistema creado para la universidad Marista.">
    <meta name="keywords" content="Colaboradores, Angel Guillermo Hernandez Zambrano, Ricardo Ascencio Flores, Berenice Villa Fuerte, Gustavo, Julieta Denisse, Oscar Vallejo Tapia">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <link rel="icon" href="../styles/images/favicon/favi.ico" sizes="32x32">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>@yield('titulo')</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
     <link type="text/css" rel="stylesheet" href="../styles/plugins/datatable/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../styles/css/isolata-font.css" type="text/css">
    <link rel="stylesheet" href="../styles/css/icon.css" type="text/css">
    <!-- ============================-->
    <!-- CSS-->
    <link rel="stylesheet" href="../styles/css/plugins/css-datepicker/mia.min.css">
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/main.css">
    <link rel="stylesheet" href="../styles/css/dynamic.min.css">
    <link rel="stylesheet" href="../styles/css/markup.min.css">
    <link rel="stylesheet" href="../styles/plugins/scrollbar/perfect-scrollbar.min.css">
  </head>
  <body>
    <!-- ============================-->
    <!-- PRELOAD-->
    <!-- ============================-->
    <div id="preloader">
      <div class="preloader-center">
        <div class="dots-loader dot-circle"></div>
      </div>
  </div>
    <!-- ============================-->
    <!-- CONTENT AREA-->
    <!-- ============================-->
    <header>
      <!-- ============================-->
      <!-- TOP NAV-->
      <!-- ============================-->
      <div class="navbar-fixed full-top-nav">
        <div id="current-menu" data-menu="default">
          <nav>
              <a class="morph menu small mob-menu button-collapse top-nav waves-effect waves-light circle hide-on-large-only" href="javascript:void(0)" id="sidebar-default-collapse" data-activates="nav-default"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span>
              </a>
            <div class="nav-wrapper">
            <!-- LOGO Set-->
              <a class="animated brand-logo hide-on-large-only nav-logo" href="javascript:void(0)"><span class="left" style="margin-left:20px;">UMVALLA</span></a>
              <a class="animated brand-logo hide-on-med-and-down defaultMenu-logo" href="javascript:void(0)">
              <span class="left" style="margin-left:20px;"><img class="responsive-img" src="../styles/images/lg-instituto-valladolid.svg"></span></a>
              <!--Left menu options at top-nav-->
              <ul class="left topnav-Menu-ls hide-on-med-and-down">
                <li><a class="morph small iconizedToggle waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
              </ul>
              <!-- Right Menu-->
              <ul class="right">
                <li class="hide-on-med-and-down"><a class="waves-effect waves-set app-search-btn" href="#"></li>
                <!-- MESSAGE SECTION-->
                <li class="hide-on-med-and-down"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgsweb"></a></li>
                <!-- ADMIN SETTINGS SECTION-->
                <li><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-userProfile"><img class="circle admin-profile-img-small" src="../images/placeholder/50x50g.jpg" alt=""></a></li>
                <li class="hide-on-med-and-down"><a class="morph notify small notification-toggle-open waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
                <li class="hide-on-large-only"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="javascript:void(0);"><i class="material-icons">more_vert</i></a></li>
              </ul>
              <!-- Mobile Screen Nav Options-->
              <ul class="right hide-on-large-only topnav-hidden-menu hide">
                <li></li>
                <li class="right"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="#"><i class="material-icons">more_vert</i></a></li>
                <li class="right"><a class="morph notify small notification-toggle-open waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
                <li class="right"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgs"></a></li>
              </ul>
              <!-- DROP-DOWN-->
              <div class="drop-down-bucket">
                  <li class="collection-item msg-footer pos-relative"></li>
             <!-- Searchbar-->
              <form class="inactive animated" id="app-search">

              </form>
          </nav>
        </div>
      </div>
      <!-- ============================-->
      <!-- Vertical Navigation (Default and Iconized)-->
      <!-- ============================-->
      <div class="vertical-navigations animated">
        <ul class="side-nav fixed animated collapsible collapsible-accordion" id="nav-default">
            <li class="logo"><a class="brand-logo hide-on-large-only" id="logo-container" href="#"></a></li>
            <li class="usr-profile">
              <div class="usr-profile-header"><a href="#"><img class="circle" src="../styles/images/placeholder/50x50g.jpg" alt="Thor"></a></div>
              <ul class="user-options">
                <li class="waves-effect waves-set"><span class="usr-name">Usuario Marista</span></li>
              </ul>
            </li>
            <li><a class="collapsible-header no-col-body waves-effect waves-set active current" href="{{ route('acceso') }}"><i class="material-icons">home</i><span>Inicio</span></a></li>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">list</i><span>Administración</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item">
                    <ul class="collapsible" data-collapsible="expandable">
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">person</i><span>Usuarios</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('FA') }}"><i class="material-icons right">perm_identity</i><span>Estudiantes</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('FP') }}"><i class="material-icons right">perm_identity</i><span>Profesores</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('FC') }}"><i class="material-icons right">perm_identity</i><span>Coordinadores</span></a></li>
                            <ul class="collapsible" data-collapsible="expandable">
                                <li>
                                    <div class="collapsible-header waves-effect waves-set"><i class="material-icons">list</i><span>Ver</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                                    <div class="collapsible-body">
                                        <ul>
                                            <li><a class="waves-effect waves-set" href="{{ route('GF') }}"><i class="material-icons">perm_identity</i><span>Grupos</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                           </li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
            </div>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">list</i><span>Consultas</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i>
            </a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item">
                    <ul class="collapsible" data-collapsible="expandable">
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">list</i><span>His. Alumnos</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('BLA') }}"><i class="material-icons">assignment</i><span>Boletas</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('Kardex') }}"><i class="material-icons">assignment</i><span>Kardex</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('hist') }}"><i class="material-icons">assignment</i><span>Historicos</span></a></li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">list</i><span>Oferta Educativa</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i>
            </a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item">
                    <ul class="collapsible" data-collapsible="expandable">
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">list</i><span>Inf.Esc.</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('PS') }}"><i class="material-icons">picture_in_picture</i><span>Plan Estudios</span></a></li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </li>
            <li class="navigation-header"><span class="no-col-body">Configuraciones</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Components">more_horiz</i>
            </li>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>Configuración</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i>
            </a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item"><a class="waves-effect waves-set" href="{{ route('rs') }}"><i class="material-icons">assignment</i><span>Roles y Permisos</span></a></li>
                </ul>
              </div>
            </li>
        </ul>
      </div>
      <!-- ============================-->
      <!-- Horizontal Navigation (Default and Iconized)-->
      <!-- ============================-->
      <div class="other-verticalSections">
        <!-- ============================-->
        <!-- RIGHT SIDEBAR NOTIFICATION SECTION-->
        <!-- ============================-->
        <!-- NOTIFICATION-SIDEBAR-->
        <div class="row notification-sidebar fixed animated" id="sb-notification">
          <div class="col s12">
            <ul class="tabs rsb-notifications">
              <li class="tab col s12"><a href="#rsb-shortcuts"><i class="material-icons small">widgets</i></a></li>
            </ul>
            <div class="col s12" id="rsb-shortcuts">
              <ul class="tab-shortcut collection" id="psTabShortcut">
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="#"><i class="material-icons yellow-text">assignment_ind</i><span class="shortcut-name">Calendario</span></a></li>
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="#"><i class="material-icons cyan-text">settings</i><span class="shortcut-name">Configuraciones</span></a></li>
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="#"><i class="material-icons amber-text">perm_identity</i><span class="shortcut-name">Perfil</span></a></li>
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="{{ route('regresar') }}"><i class="material-icons red-text">power_settings_new</i><span class="shortcut-name">Cerrar Sesión</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <main class="animated">
      <ul class="dropdown-content action-ex-opts" id="generalDropDown">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
      </ul>
      <!-- ============================-->
      <!-- MAIN HEADER-->
      <!-- ============================-->
      <div class="main-header">
        @yield('titulos-cabezera')
      </div>
      <!-- ============================-->
      <!-- Main container-->
      <!-- ============================-->
      <div class="main-container">
        @yield('contenido')
      </div>
      <!-- FOOTER-->
      <div class="row">
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
      </div>
      <footer class="page-footer no-mrpd grey lighten-4 center" >
        <div class="footer-copyright">
          <div class="container primary-text"><div class="container primary-text"><a href="{{ route('creditos') }}">Creditos Equipo de Desarrollo ITGS</a>
          </div>© 2019 Universidad Marista Valladolid, Todos los derechos reservados.
          </div>
        </div>
      </footer>
    </main>
    <!-- ============================-->
    <!-- SCRIPTS-->
    <script type="text/javascript" src='../styles/plugins/js-datepicker/datepicker.min.js'></script>
 <script type="text/javascript" src='../styles/plugins/js-datepicker/miquerypicker.min.js'></script>
 <script type="text/javascript" src="../styles/plugins/js-datepicker/index.js"></script>
    <!-- ============================-->
    <script type="text/javascript" src="../styles/js/all.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/advanced.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/basic.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/dataTables.select.min.js"></script>
  <script type="text/javascript" src="../styles/plugins/datatable/templates.js"></script>
  <script type="text/javascript" src="../styles/plugins/datatable/using-api.js"></script>
  <script type="text/javascript" src="../styles/plugins/datatable/buscador.js"></script>
  <script>
    $(document).ready(function(){
      $('select').material_select();
    });
  </script>
  <script>
  $(document).ready(function(){
$("#searchClave").keyup(function(){
_this = this;
// Show only matching TR, hide rest of them
$.each($("#mytable tbody tr"), function() {
if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
$(this).hide();
else
$(this).show();
});
});
});
</script>
<script>
$(document).ready(function(){
$("#searchGrupo").keyup(function(){
_this = this;
// Show only matching TR, hide rest of them
$.each($("#tablaGrupo tbody tr"), function() {
if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
$(this).hide();
else
$(this).show();
});
});
});
</script>
     </body>
</html>
