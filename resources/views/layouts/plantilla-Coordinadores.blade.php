<!DOCTYPE html>
<html class="loading" lang="es">
  <head>
    <link rel="icon" href="../images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../images/favicon/favicon-32x32.png">
    <!-- ============================-->
    <!-- META DATA-->
    <!-- ============================-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sistema creado para la universidad Marista.">
    <meta name="keywords" content="Colaboradores, Angel Guillermo Hernandez Zambrano, Ricardo Ascencio Flores, Berenice Villa Fuerte, Gustavo, Julieta Denisse, Oscar Vallejo Tapia">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <link rel="icon" href="../styles/images/favicon/favi.ico" sizes="32x32">
    <meta name="theme-color" content="#2a56c6">
    <!-- ============================-->
    <!-- TITLE-->
    <!-- ============================-->
    <title>@yield('titulo')</title>
    <!-- ============================-->
    <!-- FONTS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/isolata-font.css" type="text/css">
    <link rel="stylesheet" href="../styles/css/icon.css" type="text/css">
    <!-- ============================-->
    <!-- CSS-->
    <!-- ============================-->
    <link rel="stylesheet" href="../styles/css/main.css">
    <link rel="stylesheet" href="../styles/css/dynamic.min.css">
    <link rel="stylesheet" href="../styles/css/markup.min.css">
    <link rel="stylesheet" href="../styles/plugins/scrollbar/perfect-scrollbar.min.css">
  </head>
  <body>
    <!-- ============================-->
    <!-- PRELOAD-->
    <!-- ============================-->
    <div id="preloader">
      <div class="preloader-center">
        <div class="dots-loader dot-circle"></div>
      </div>
    </div>
    <!-- ============================-->
    <!-- CONTENT AREA-->
    <!-- ============================-->
    <header>
      <!-- ============================-->
      <!-- TOP NAV-->
      <!-- ============================-->
      <div class="navbar-fixed full-top-nav">
        <div id="current-menu" data-menu="default">
          <nav><a class="morph menu small mob-menu button-collapse top-nav waves-effect waves-light circle hide-on-large-only" href="javascript:void(0)" id="sidebar-default-collapse" data-activates="nav-default"><span><span class="s1"></span></a>
            <div class="nav-wrapper">

              <a class="animated brand-logo hide-on-large-only nav-logo" href="javascript:void(0)"><span class="left" style="margin-left:20px;">UMVALLA</span></a>
              <a class="animated brand-logo hide-on-med-and-down defaultMenu-logo" href="javascript:void(0)">
              <span class="left" style="margin-left:20px;"><img class="responsive-img" src="../styles/images/lg-instituto-valladolid.svg"></span></a>
              <!-- Left menu options at top-nav-->
              <ul class="left topnav-Menu-ls hide-on-med-and-down">
                <li><a class="morph small iconizedToggle waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
              </ul>
              <!-- Right Menu-->
              <ul class="right">
                <li class="hide-on-med-and-down"><a class="waves-effect waves-set app-search-btn" href="#"></li>
                <!-- MESSAGE SECTION-->
                <li class="hide-on-med-and-down"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgsweb"></a></li>
                <!-- ADMIN SETTINGS SECTION-->
                <li><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-userProfile"><img class="circle admin-profile-img-small" src="../images/placeholder/50x50g.jpg" alt=""></a></li>
                <li class="hide-on-med-and-down"><a class="morph notify small notification-toggle-open waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
                <li class="hide-on-large-only"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="javascript:void(0);"><i class="material-icons">more_vert</i></a></li>
              </ul>
              <!-- Mobile Screen Nav Options-->
              <ul class="right hide-on-large-only topnav-hidden-menu hide">
                <li></li>
                <li class="right"><a class="waves-effect waves-set toggle-topnav-hidden-menu" href="#"><i class="material-icons">more_vert</i></a></li>
                <li class="right"><a class="morph notify small notification-toggle-open waves-effect waves-light" href="javascript:void(0)"><span><span class="s1"></span><span class="s2"></span><span class="s3"></span></span></a>
                </li>
                <li class="right"><a class="dropdown-button waves-effect waves-set" href="#" data-beloworigin="true" data-activates="top-nav-msgs"></a></li>
              </ul>
              <!-- DROP-DOWN-->
              <div class="drop-down-bucket">
                  <li class="collection-item msg-footer pos-relative"></li>
             <!-- Searchbar-->
              <form class="inactive animated" id="app-search">

              </form>
          </nav>
        </div>
      </div>
      <!-- ============================-->
      <!-- Vertical Navigation (Default and Iconized)-->
      <!-- ============================-->
      <div class="vertical-navigations animated">
        <ul class="side-nav fixed animated collapsible collapsible-accordion" id="nav-default">
            <li class="usr-profile">
              <div class="usr-profile-header"><a href="#"><img class="circle" src="../styles/images/placeholder/50x50g.jpg" alt="Thor"></a></div>
              <ul class="user-options">
                <li class="waves-effect waves-set"><span class="usr-name">Usuario Marista</span></li>
              </ul>
            </li>
            <li><a class="collapsible-header no-col-body waves-effect waves-set active current" href="{{ route('Inicio') }}"><i class="material-icons">home</i><span>Inicio</span></a></li>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">list</i><span>Perfil</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i>
            </a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item">
                    <ul class="collapsible" data-collapsible="expandable">
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">perm_identity</i><span>Mis Datos</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('Datos') }}"><i class="material-icons">perm_identity</i><span>Personales</span></a></li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header waves-effect waves-set" href="#"><i class="material-icons">list</i><span>Consultas</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i>
            </a>
              <div class="collapsible-body">
                <ul>
                  <li class="menu-item">
                    <ul class="collapsible" data-collapsible="expandable">
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">library_books</i><span>Periodo Actual</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('cal') }}"><i class="material-icons">picture_in_picture</i><span>Calificaciones</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('Horario') }}"><i class="material-icons">picture_in_picture</i><span>Horarios</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('grupoCoordinador') }}"><i class="material-icons">picture_in_picture</i><span>Grupos</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('Adeudos') }}"><i class="material-icons">picture_in_picture</i><span>Adeudos</span></a></li>
                          </ul>
                        </div>
                      </li>
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">collections_bookmark</i><span>Historico</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('plan') }}"><i class="material-icons">picture_in_picture</i><span>Planes de Estudios</span></a></li>
                            <li><a class="waves-effect waves-set" href="{{ route('KA') }}"><i class="material-icons">picture_in_picture</i><span>Kardex de Alumnos</span></a></li>
                          </ul>
                        </div>
                      </li>
                      <li>
                        <div class="collapsible-header waves-effect waves-set"><i class="material-icons">collections_bookmark</i><span>Profesores</span><i class="material-icons mdi-navigation-chevron-left">keyboard_arrow_left</i></div>
                        <div class="collapsible-body">
                          <ul>
                            <li><a class="waves-effect waves-set" href="{{ route('profesoresCoordinador') }}"><i class="material-icons">picture_in_picture</i><span>Activos</span></a></li>                            
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </li>
        </ul>
      </div>
      <!-- ============================-->
      <!-- Horizontal Navigation (Default and Iconized)-->
      <!-- ============================-->
      <div class="horizontal-navigations navbar-fixed full-top-nav">
        <!--<nav class="hide-on-med-and-down">
          <div class="nav-wrapper">
            <ul class="iconized left">
              <li class="menu-item"><a class="menu-link waves-effect waves-set" href="index.html"><i class="material-icons menu-icon left">dashboard</i><span class="menu-span">Dashboard</span></a></li>
              <li class="menu-item"><a class="open_hor_sub menu-link waves-effect waves-set" href="#"><i class="material-icons menu-icon left">wb_sunny</i><span class="menu-span">Widgets</span><i class="material-icons menu-icon right">arrow_drop_down</i></a>
                <div class="hor_sub standard hide">
                  <ul class="collection collapsible collapsible-accordion">
                    <li class="collection-item"><a class="waves-effect waves-set" href="card-statistic.html">Statistic Card<i class="material-icons left">show_chart</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="card-weather.html">Weather Card<i class="material-icons left">wb_sunny</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="card-feeds.html">Feeds Card<i class="material-icons left">view_list</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="card-travel.html">Travel Card<i class="material-icons left">panorama_fish_eye</i></a></li>
                  </ul>
                </div>
              </li>
              <li class="menu-item"><a class="active open_hor_sub menu-link waves-effect waves-set" href="#"><i class="material-icons menu-icon left">insert_chart</i><span class="menu-span">CSS Properties</span><i class="material-icons menu-icon right">arrow_drop_down</i></a>
                <div class="hor_sub standard hide">
                  <ul class="collection collapsible collapsible-accordion">
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-color.html">Color<i class="material-icons left">color_lens</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-css-media.html">CSS Media<i class="material-icons left">photo_filter</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-grid.html">Grid<i class="material-icons left">grid_on</i></a></li>
                    <li class="collection-item"><a class="collapsible-header waves-effect waves-set" href="#">Tables<i class="mdi mdi-table left"></i><i class="material-icons right">arrow_drop_down</i></a>
                      <div class="collapsible-body">
                        <ul>
                          <li><a class="waves-effect waves-set" href="materialize-table.html">Materialize Table<i class="material-icons left">list</i></a></li>
                          <li><a class="waves-effect waves-set" href="datatable-basic.html">Datatable Basic Table<i class="material-icons left">format_list_bulleted</i></a></li>
                          <li><a class="waves-effect waves-set" href="datatable-basic-style.html">Datatable Basic Style<i class="material-icons left">format_paint</i></a></li>
                          <li><a class="waves-effect waves-set" href="datatable-ordering.html">Datatable Sort Datatable<i class="material-icons left">sort</i></a></li>
                          <li><a class="waves-effect waves-set" href="datatable-advance.html">Advance Datatable<i class="material-icons left">star</i></a></li>
                          <li><a class="waves-effect waves-set" href="datatable-template.html">Datatable Templates<i class="material-icons left">style</i></a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-helper.html">Helpers<i class="material-icons left">help</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-shadow.html">Shadow<i class="material-icons left">queue</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-typography.html">Typography<i class="material-icons left">text_format</i></a></li>
                    <li class="collection-item active"><a class="active waves-effect waves-set" href="materialize-icons.html">Icon<i class="material-icons left">gesture</i></a></li>
                  </ul>
                </div>
              </li>
              <li class="menu-item"><a class="open_hor_sub menu-link waves-effect waves-set" href="#"><i class="material-icons menu-icon left">format_paint</i><span class="menu-span">Components</span><i class="material-icons menu-icon right">arrow_drop_down</i></a>
                <div class="hor_sub full hide">
                  <ul class="collection">
                    <li class="collection-header">
                      <h6>UI Elements</h6>
                    </li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-badge.html">Badges<i class="material-icons left">turned_in</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-button.html">Button<i class="material-icons left">touch_app</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-breadcrumbs.html">Breadcrumbs<i class="material-icons left">swap_horiz</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-cards.html">Cards<i class="material-icons left">card_membership</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-chips.html">Chips<i class="material-icons left">label_outline</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-collection.html">Collections<i class="material-icons left">view_list</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-pagination.html">Pagination<i class="material-icons left">last_page</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-preloader.html">Preloader<i class="material-icons left">cached</i></a></li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="form-components">Form Components<i class="material-icons left">transform</i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="form-components">
                        <li><a class="waves-effect waves-set" href="form-basic-components.html">Basic Inputs<i class="material-icons left">format_align_center</i></a></li>
                        <li><a class="waves-effect waves-set" href="form-checkbox-radios.html">Checkboxes & Radios<i class="material-icons left">check_box</i></a></li>
                        <li><a class="waves-effect waves-set" href="form-extended-control.html">Extended Control<i class="material-icons left">more</i></a></li>
                        <li><a class="waves-effect waves-set" href="form-select.html">Select Inputs<i class="material-icons left">select_all</i></a></li>
                        <li><a class="waves-effect waves-set" href="form-taginput.html">Tag Input<i class="material-icons left">label_outline</i></a></li>
                        <li><a class="waves-effect waves-set">Advance Input<i class="material-icons left">power_input</i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="collection">
                    <li class="collection-header">
                      <h6>Advance UI</h6>
                    </li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-collapsible.html">Collapsible<i class="material-icons left">collections</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-dialogs.html">Dialogs<i class="material-icons left">notifications_active</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-dropdown.html">Dropdown<i class="material-icons left">arrow_drop_down_circle</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-media.html">Media<i class="material-icons left">perm_media</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-carousel.html">Carousel<i class="material-icons left">view_carousel</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-modals.html">Modals<i class="material-icons left">laptop_windows</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-pushpin.html">PushPin<i class="material-icons left">view_list</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-scrollFire.html">ScrollFire<i class="material-icons left">all_inclusive</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-tabs.html">Tabs<i class="material-icons left">label</i></a></li>
                  </ul>
                  <ul class="collection">
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-transitions.html">Transactions<i class="material-icons left">stars</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="materialize-waves.html">Waves<i class="material-icons left">polymer</i></a></li>
                    <li class="collection-header">
                      <h6>Form Component</h6>
                    </li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="form-basic-components.html">Basic Inputs<i class="material-icons left">format_align_center</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="form-checkbox-radios.html">Checkboxes & Radios<i class="material-icons left">check_box</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="form-extended-control.html">Extended controls<i class="material-icons left">more</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="form-select.html">Select inputs<i class="material-icons left">select_all</i></a></li>
                    <li class="collection-item"><a class="waves-effect waves-set" href="form-taginput.html">Tag Input<i class="material-icons left">label_outline</i></a></li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="advance-inputs">Advance Inputs<i class="material-icons left">transform</i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="advance-inputs">
                        <li><a class="waves-effect waves-set" href="editor-materializenote.html">MaterializeNote<i class="material-icons left">note</i></a></li>
                        <li><a class="waves-effect waves-set" href="editor-ckeditor.html">CK Editor<i class="material-icons left">keyboard</i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="picker-inputs">Pickers<i class="material-icons left">invert_colors</i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="picker-inputs">
                        <li class="menu-item"><a class="waves-effect waves-set" href="picker-datetime.html">Date-Time<i class="material-icons left">date_range</i></a></li>
                        <li class="menu-item"><a class="waves-effect waves-set" href="picker-color.html">Color<i class="material-icons left">color_lens</i></a></li>
                        <li class="menu-item"><a class="waves-effect waves-set" href="picker-location.html">Location<i class="material-icons left">pin_drop</i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="collection">
                    <li class="collection-header">
                      <h6>Charts</h6>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="bar-charts-collection">Bar-Charts<i class="mdi mdi-chart-bar left"></i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="bar-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-barchart.html">Google Chart<i class="mdi mdi-google left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="c3-barchart.html">c3 Chart<i class="material-icons left">multiline_chart</i></a></li>
                        <li><a class="waves-effect waves-set" href="chartjs-barchart.html">ChartJs<i class="mdi mdi-image-filter-hdr left"></i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="line-charts-collection">Line-Charts<i class="mdi mdi-chart-line left"></i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="line-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-linechart.html">Google Chart<i class="mdi mdi-google left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="c3-linechart.html">c3 Chart<i class="material-icons">multiline_chart</i></a></li>
                        <li><a class="waves-effect waves-set" href="chartjs-linechart.html">ChartJs<i class="mdi mdi-image-filter-hdr left"></i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="area-charts-collection">Line-Charts<i class="mdi mdi-chart-areaspline left"></i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="area-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-areachart.html">Google Chart<i class="mdi mdi-google left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="c3-areachart.html">c3 Chart<i class="material-icons left">multiline_chart</i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="pieChart-charts-collection">Line-Charts<i class="material-icons left">pie_chart</i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="pieChart-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-piechart.html">Google Chart<i class="mdi mdi-google left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="c3-piechart.html">c3 Chart<i class="material-icons left">multiline_chart</i></a></li>
                        <li><a class="waves-effect waves-set" href="chartjs-pie.html">Chart Js<i class="mdi mdi-image-filter-hdr left"></i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="scatterChart-charts-collection">Scatter Charts<i class="mdi mdi-chart-scatterplot-hexbin left"></i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="scatterChart-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-scatterchart.html">Google Chart<i class="mdi mdi-google left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="chartjs-scatter.html">Chart JS<i class="mdi mdi-image-filter-hdr left"></i></a></li>
                      </ul>
                    </li>
                    <li class="collection-item"><a class="dropdown-button waves-effect waves-set" href="#" data-activates="Extra-charts-collection">Extra<i class="material-icons left">more</i><i class="material-icons right">arrow_drop_down</i></a>
                      <ul class="dropdown-content" id="Extra-charts-collection">
                        <li><a class="waves-effect waves-set" href="google-columnchart.html">Google Column Chart<i class="material-icons left">view_column</i></a></li>
                        <li><a class="waves-effect waves-set" href="google-combochart.html">Google Combo Chart<i class="mdi mdi-chart-gantt left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="google-geocharts.html">Google Geochart<i class="mdi mdi-earth left"></i></a></li>
                        <li><a class="waves-effect waves-set" href="c3-more.html">C3 Extra<i class="material-icons left">multiline_chart</i></a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
      </nav>-->
      </div>
      <div class="other-verticalSections">
        <!-- ============================-->
        <!-- RIGHT SIDEBAR NOTIFICATION SECTION-->
        <!-- ============================-->
        <!-- NOTIFICATION-SIDEBAR-->
        <div class="row notification-sidebar fixed animated" id="sb-notification">
          <div class="col s12">
            <ul class="tabs rsb-notifications">
              <li class="tab col s12"><a href="#rsb-shortcuts"><i class="material-icons small">widgets</i></a></li>
            </ul>
            <div class="col s12" id="rsb-shortcuts">
              <ul class="tab-shortcut collection" id="psTabShortcut">
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="{{ route('Datos') }}"><i class="material-icons amber-text">perm_identity</i><span class="shortcut-name">Perfil</span></a></li>
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="#"><i class="material-icons yellow-text">assignment_ind</i><span class="shortcut-name">Calendario</span></a></li>
                <!--<li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="#"><i class="material-icons cyan-text">settings</i><span class="shortcut-name">Configuraciones</span></a></li>-->
                <li class="collection-item waves-effect waves-set"><a class="shortcut-aItem" href="{{ route('regresar') }}"><i class="material-icons red-text">power_settings_new</i><span class="shortcut-name">Cerrar Sesión</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <main class="animated">
      <!--<ul class="dropdown-content action-ex-opts" id="generalDropDown">
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">account_box</i><span>Account</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">local_activity</i><span>Recent Activities</span></a></li>
        <li><a class="waves-effect waves-set" href="#"><i class="mdi mdi-wheelchair-accessibility"></i><span>Accessibility</span></a></li>
        <li class="divider"></li>
        <li><a class="waves-effect waves-set" href="#"><i class="material-icons">settings</i><span>All Settings</span></a></li>
    </ul>-->
      <!-- ============================-->
      <!-- MAIN HEADER-->
      <!-- ============================-->
      <div class="main-header">
        @yield('titulos-cabezera')
      </div>
      <!-- ============================-->
      <!-- Main container-->
      <!-- ============================-->
      <div class="main-container">
        @yield('contenido')
      </div>
      <!-- FOOTER-->
      <div class="row">
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
      </div>
      <footer class="page-footer no-mrpd grey lighten-4 center" >
        <div class="footer-copyright">
          <div class="container primary-text"><div class="container primary-text"><a href="{{ route('creditos') }}">Creditos Equipo de Desarrollo ITGS</a>
          </div>
              © 2019 Universidad Marista Valladolid, Todos los derechos reservados.
          </div>
        </div>
      </footer>
    </main>
    <!-- ============================-->
    <!-- SCRIPTS-->
    <!-- ============================-->
    <script type="text/javascript" src="../styles/js/all.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/advanced.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/basic.js"></script>
    <script type="text/javascript" src="../styles/plugins/datatable/dataTables.select.min.js"></script>
  <script type="text/javascript" src="../styles/plugins/datatable/templates.js"></script>
  <script type="text/javascript" src="../styles/plugins/datatable/using-api.js"></script>
  <script>
    $(document).ready(function(){
      $('select').material_select();
    });
  </script>
  </body>
</html>
