<!doctype html>
<html lang="{{ app()->getLocale() }}" class="loading">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>UMVALLA:: Página Principal</title>
        <!-- Fonts -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon/favi.ico">
        <!--favicon-->
        <link href="../styleb/Roboto" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="../css/inicio.css">
        <link rel="stylesheet" type="text/css" href="../styleb/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/materialize.css">
        <link rel="stylesheet" href="../styles/css/main.css">
        <link rel="stylesheet" href="../styles/css/isolata-font.css" type="text/css">
        <link rel="stylesheet" href="../styles/css/icon.css" type="text/css">
        <link rel="stylesheet" href="../styles/css/markup.min.css">

    </head>
    <body>
        <div id="preloader">
          <div class="preloader-center">
            <div class="dots-loader dot-circle"></div>
          </div>
      </div>
      <div class="banner">
          <div class="bannerTxt">
              SGE-UMVALLA
          </div>
          <div class="main-container">
              <img class="img-fluid" alt="Responsive image" src="../images/UNI2.png">
          </div>
      </div>

      <div class="main-container">
          <div class="content">
              <div class="card light-blue darken-4 hoverable">
                <div class="card-tabs">
                  <ul class="tabs tabs-fixed-width tabs-transparent">
                    <li class="tab"><a class="active" href="#ControlEsc">Control Escolar</a></li>
                    <li class="tab"><a href="#Coordinadores">Coordinadores</a></li>
                    <li class="tab"><a href="#Profesores">Profesores</a></li>
                     <li class="tab"><a href="#Alumnos">Alumnos</a></li>
                  </ul>
                </div>
                <div class="card-content blue lighten-4 center">
                  <div id="ControlEsc">
                      <div class="row">
                          <form class="col m6 s12" method="post" action="#">
                                  <div class="input-field col m6 s12">
                                      <input placeholder="Nombre de usuario" id="clav" type="text" class="validate"pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                                  <div class="input-field col m6 s12">
                                      <input type="password" placeholder="Contraseña" id="psw" type="text" class="validate" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                              <p class="center">
                                  <!--<button class="waves-effect waves-light btn-small" type="submit" name="action">Ingresar<i class="material-icons right">input</i></button>-->

                                   <a class="waves-effect waves-light btn-small" href="{{ route('inicioCE') }}"><i class="material-icons right">input</i>Iniciar Sesión</a>
                              </p>
                          </form>
                      </div>
                  </div>
                  <div id="Coordinadores">
                      <div class="row">
                          <form class="col m6 s12" method="post" action="#">
                                  <div class="input-field col m6 s12">
                                      <input placeholder="Nombre de usuario" id="clav" type="text" class="validate"pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                                  <div class="input-field col m6 s12">
                                      <input type="password" placeholder="Contraseña" id="psw" type="text" class="validate" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                              <p class="center">
                                  <!--<button class="waves-effect waves-light btn-small" type="submit" name="action">Ingresar<i class="material-icons right">input</i></button>-->
                                   <a class="waves-effect waves-light btn-small" href="{{ route('inicioC') }}"><i class="material-icons right">input</i>Iniciar Sesión</a>
                              </p>
                          </form>
                      </div>
                  </div>
                  <div id="Profesores">
                      <div class="row">
                          <form class="col m6 s12" method="post" action="#">
                                  <div class="input-field col m6 s12">
                                      <input placeholder="Nombre de usuario" id="clav" type="text" class="validate"pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                                  <div class="input-field col m6 s12">
                                      <input type="password" placeholder="Contraseña" id="psw" type="text" class="validate" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                  </div>
                              <p class="center">
                                 <!--<button class="waves-effect waves-light btn-small" type="submit" name="action">Ingresar<i class="material-icons right">input</i></button>-->
                                  <a class="waves-effect waves-light btn-small" href="{{ route('inicioP') }}"><i class="material-icons right">input</i>Iniciar Sesión</a>
                              </p>
                          </form>
                      </div>
                  </div>
                  <div id="Alumnos">
                      <div class="row">
                              <form class="col m6 s12" method="post" action="#">
                                      <div class="input-field col m6 s12">
                                          <input placeholder="Clave de Acceso" id="clav" type="text" class="validate"pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                      </div>
                                      <div class="input-field col m6 s12">
                                          <input type="password" placeholder="Contraseña" id="psw" type="text" class="validate" pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ\ s]{3,30}" required title="Formato: De 3 a 30 letras">
                                      </div>
                                  <p class="center">
                                      <!--<button class="waves-effect waves-light btn-small" type="submit" name="action">Ingresar<i class="material-icons right">input</i></button>-->
                                      <a class="waves-effect waves-light btn-small" href="{{ route('inicioA') }}"><i class="material-icons right">input</i>Iniciar Sesión</a>
                                  </p>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
<div class="footer">

    <a href="{{ route('creditos') }}">Creditos Equipo de Desarrollo ITGS</a>

    <br>
    <br>
</div>
        <script src="../styleb/js/bootstrap.bundle.js"></script>
        <script src="../styleb/js/bootstrap.bundle.js.map"></script>
        <script src="../styleb/js/bootstrap.bundle.min.js"></script>
        <script src="../styleb/js/bootstrap.bundle.mim.js.map"></script>
        <script src="../styleb/js/bootstrap.js"></script>
        <script src="../styleb/js/bootstrap.js.map"></script>
        <script src="../styleb/js/bootstrap.min.js"></script>
        <script src="../styleb/js/bootstrap.min.js.map"></script>
        <!-- SCRIPTS-->
        <!-- ============================-->
        <script type="text/javascript" src="../styles/js/all.js"></script>
        <script type="text/javascript" src="../styles/plugins/charts/chartjs/dist/Chart.bundle.js"></script>
        <script type="text/javascript" src="../styles/plugins/sparkline/jquery.sparkline.js"></script>
        <script type="text/javascript" src="../styles/plugins/charts/google-chart/loader.js"></script>
        <script type="text/javascript" src="../styles/plugins/charts/google-chart/jsapi.js"></script>
        <!--<script src="../js/dashboard.js"></script>-->
        <script type="text/javascript" src="../js/all.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
    </body>
</html>
